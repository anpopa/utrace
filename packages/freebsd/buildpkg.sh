#!/bin/sh

usage ()
{
    echo "Usage: $0 <manifest_template> <files_directory> <outdir>"
    exit
}

if [ "$#" -ne 3 ]
then
        usage
fi
    
manifest_template=$1
files_dir=$2
out_dir=$3
PSIZE=$(find ${files_dir} -type f -exec stat -f %z {} + | awk 'BEGIN {s=0} {s+=$1} END {print s}')
export PSIZE
{
    . ${manifest_template}
    # Add files in
    echo "files = {"
    find ${files_dir} -type f -exec sha256 -r {} + |
        awk '{print "    " $2 " = " "\"" $1 "\""}'
    # Add symlinks in
    find ${files_dir} -type l |
        awk "{print \"    \" \$1 \" = '-'\"}"
        
    echo "}"
    # Add files_directories in
    echo "files_directories = {"
    find ${files_dir} -type d -mindepth 1 |
        awk '{print "    " $1 "= y"}'
    echo "}"
            
} | sed -e "s:${files_dir}::" > ${out_dir}/+MANIFEST
        
# Create the package
pkg create -r ${files_dir} -m ${out_dir} -o ${out_dir}
