/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef FILETRANSFER_H
#define FILETRANSFER_H

#include "ut_types.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct file {
    FILE *ftmp;
    FILE *fnew;

    char name[UT_FILENAME_LEN];
    char path[UT_FILENAME_LEN];
    char ptmp[UT_FILENAME_LEN];
    char pnew[UT_FILENAME_LEN];

    uint32_t id;
    uint64_t fsize;
    uint64_t tsize;
    uint64_t tframes;
} file_t;

typedef struct transfer {
    bool run;
    file_t *file;
} transfer_t;

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */
int transfer_init(transfer_t *ft);

int transfer_set_path(transfer_t *ft, const char *path);

int transfer_start(transfer_t *ft, utframe_t *frame);

int transfer_stop(transfer_t *ft, utframe_t *frame);

int transfer_uninit(transfer_t *ft);

int transfer_push_data(transfer_t *ft, utframe_t *frame);

#endif /* FILETRANSFER_H */
