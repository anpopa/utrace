/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "utracemon.h"
#include <ut_serialize.h>
#include <ut_types.h>

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */
static utracemon_t *g_utracemon;

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static void utracemon_print_header(void);

static void utracemon_print_record(utrecord_t *record);

static int utracemon_connect_target(utracemon_t *dec);

static bool utracemon_validate_frame(utframe_t *frame);

static int utracemon_recv_frame(utracemon_t *dec, utframe_t *frame);

static void utracemon_print_textline(utframe_t *frame, char *appname, uint32_t count);

static void utracemon_print_file_marker(utframe_t *frame, uint32_t count);

static void utracemon_print_file_payload(utframe_t *frame, uint32_t count);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_init
 *  Description:  Init utracemon data
 * =====================================================================================
 */
static int utracemon_init(utracemon_t *dec)
{
    if (transfer_init(&dec->ft)) {
        return (-1);
    }

    dec->dfn = (char *)malloc(UT_FILENAME_LEN + 1);
    dec->ddn = (char *)malloc(UT_FILENAME_LEN + 1);
    dec->taddr = (char *)malloc(UT_FILENAME_LEN + 1);
    dec->sfd = -1;
    dec->port = 0;
    dec->con = false;

    memset(dec->dfn, 0, UT_FILENAME_LEN + 1);
    memset(dec->ddn, 0, UT_FILENAME_LEN + 1);
    memset(dec->taddr, 0, UT_FILENAME_LEN + 1);

    return (ut_decoder_init(&dec->decoder));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_deinit
 *  Description:  Clean utracemon initialization data
 * =====================================================================================
 */
static int utracemon_deinit(utracemon_t *dec)
{
    if (dec->dfn) {
        free(dec->dfn);
    }

    if (dec->taddr) {
        free(dec->taddr);
    }

    if (dec->ddn) {
        free(dec->ddn);
    }

    ut_decoder_free(&dec->decoder);
    transfer_uninit(&dec->ft);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_build_decoder
 *  Description:  Parse decoder file
 * =====================================================================================
 */
static int utracemon_build_decoder(utracemon_t *dec)
{
    FILE *dfile;
    int status = 0;
    char *lpath = NULL;
    char *nline;

    dfile = fopen(dec->dfn, "r");
    if (!dfile) {
        return (-1);
    }

    lpath = (char *)malloc(UT_FILENAME_LEN + 1);
    if (!lpath) {
        return (-1);
    }

    while (!feof(dfile) && (status >= 0)) {
        memset(lpath, 0, sizeof(UT_FILENAME_LEN) + 1);

        if (!fgets(lpath, UT_FILENAME_LEN, dfile)) {
            break;
        }

        if ((nline = strchr(lpath, '\n')) != NULL) {
            *nline = '\0';
        }

        status = ut_decoder_add_file(&dec->decoder, lpath);

        if (status < 0) {
            printf("utracemon: cannot add decoder file at path: %s\n", lpath);
        }
    }

    free(lpath);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_decode_target
 *  Description:  Decode target file
 * =====================================================================================
 */
static int utracemon_decode_target(utracemon_t *dec)
{
    utframe_t *frame;
    utrecord_t *record;
    fd_set rfd;
    struct timeval tv;
    uint32_t count = 0;
    utpacketinfo_t *pkt;
    uint8_t i;
    char appname[UT_APPNAME_LEN];
    int status = 0;

    if (utracemon_connect_target(dec) < 0) {
        printf("utracemon: cannot connect to target at addr: %s\n", dec->taddr);
        return (-1);
    }

    frame = (utframe_t *)malloc(sizeof(utframe_t));
    record = (utrecord_t *)malloc(sizeof(utrecord_t));
    pkt = (utpacketinfo_t *)malloc(sizeof(utpacketinfo_t));

    if (!frame || !record) {
        return (-1);
    }

    utracemon_print_header();
    FD_ZERO(&rfd);

    while (dec->con) {
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        FD_SET(dec->sfd, &rfd);

        memset(frame, 0, sizeof(utframe_t));
        memset(record, 0, sizeof(utrecord_t));
        status = select(dec->sfd + 1, &rfd, NULL, NULL, &tv);
        if (status < 0) {
            printf("utracemon: disconnected from %s\n", dec->taddr);
            dec->con = false;
            status = 0;
        } else if (status) {
            status = utracemon_recv_frame(dec, frame);

            if (status < 0) {
                dec->con = false;
                break;
            } else if (utracemon_validate_frame(frame)) {
                switch (frame->typ) {
                case utfpkt:
                    for (i = 0; i < UT_FRMPKT_CNT; i++) {
                        /* If hash is zero exit for loop */
                        if (frame->pld.pks[i].hsh == 0) {
                            break;
                        }

                        memset(pkt, 0, sizeof(utpacketinfo_t));
                        pkt->aid = frame->aid;
                        pkt->pid = frame->pid;
                        pkt->hsh = frame->pld.pks[i].hsh;
                        pkt->tms = frame->tms | frame->pld.pks[i].tms;
                        memcpy(pkt->dat, &frame->pld.pks[i].dat[0], UT_PKTPLD_SZ);

                        (void)ut_decoder_apply(&dec->decoder, pkt, record);

                        record->cnt = count++;
                        utracemon_print_record(record);
                    }
                    break;

                case utffst:
                    /* Handle file transfer markers */
                    utracemon_print_file_marker(frame, count++);
                    transfer_start(&dec->ft, frame);
                    break;

                case utffso:
                    /* Handle file transfer markers */
                    utracemon_print_file_marker(frame, count++);
                    transfer_stop(&dec->ft, frame);
                    break;

                case utffpl:
                    /* Handle file transfer payload */
                    utracemon_print_file_payload(frame, count++);
                    transfer_push_data(&dec->ft, frame);
                    break;

                case utfbin:
                case utftxt:
                    memset(appname, 0, UT_APPNAME_LEN);
                    ut_decoder_get_appname(&dec->decoder, frame->aid, appname);
                    utracemon_print_textline(frame, appname, count++);
                    break;

                default:
                    break;
                }
            } else {
                /* frame corrupted */
            }
        } else {
            /* no data */
        }
    }

    free(frame);
    free(record);
    free(pkt);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_recv_frame
 *  Description:  Receive one frame
 * =====================================================================================
 */
static int utracemon_recv_frame(utracemon_t *dec, utframe_t *frame)
{
    int status = 0;
    uint32_t rsz = 0;
    fd_set rfd;
    struct timeval tv;
    uint8_t sframe[sizeof(utframe_t)];

    FD_ZERO(&rfd);
    memset(sframe, 0, sizeof(utframe_t));

    while ((rsz < sizeof(utframe_t)) && (status >= 0)) {
        status = (int)recv(dec->sfd, sframe + rsz, sizeof(utframe_t) - rsz, MSG_DONTWAIT);

        if (status > 0) {
            rsz += status;
        } else if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
            tv.tv_sec = 1;
            tv.tv_usec = 0;
            FD_SET(dec->sfd, &rfd);

            status = select(dec->sfd + 1, &rfd, NULL, NULL, &tv);
        } else {
            status = -1;
        }
    }

    if (rsz == sizeof(utframe_t)) {
        status = ut_serialize_frame_decode(sframe, frame);
    } else {
        status = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_validate_frame
 *  Description:  Check frame CRC
 * =====================================================================================
 */
static bool utracemon_validate_frame(utframe_t *frame)
{
    bool status = true;

    if (frame->sof != UTSOF) {
        status = false;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_catch_sigpipe
 *  Description:  Catch SIGPIPE
 * =====================================================================================
 */
static void utracemon_catch_sigpipe(int signum)
{
    printf("utracemon: Signal %d received, drop connection\n", signum);
    g_utracemon->con = false;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_print_header
 *  Description:  Print header line to stdout
 * =====================================================================================
 */
static void utracemon_print_header(void)
{
    printf("UTRACEDEC version: %s\n", UTRACEVERSION);
    printf("[%8s][%-20s ][%4s:%-7s][%-24s] %s\n",
           "COUNT",
           "TIMESTAMP",
           "APPID",
           "INSTANCE",
           "PAYLOAD",
           "APP NAME -> HEADLINE");
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_print_file_marker
 *  Description:  Print file transfer markers
 * =====================================================================================
 */
static void utracemon_print_file_marker(utframe_t *frame, uint32_t count)
{
    char tbuf[30];

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&frame->tms));

    if (frame->typ == utffst) {
        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %s\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "FILE_TRANSFER_START",
               "FILE",
               (char *)&frame->pld.txt[0]);
    } else if (frame->typ == utffso) {
        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %s\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "FILE_TRANSFER_END",
               "FILE",
               (char *)&frame->pld.txt[0]);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_print_file_payload
 *  Description:  Print file transfer payload frames
 * =====================================================================================
 */
static void utracemon_print_file_payload(utframe_t *frame, uint32_t count)
{
    uint32_t int1, int2, int3, int4;
    char tbuf[30];

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&frame->tms));

    if (frame->typ == utffpl) {
        int1 = (uint32_t)(frame->pld.dat[3] << 24 | frame->pld.dat[2] << 16 | frame->pld.dat[1] << 8
                          | frame->pld.dat[0]);
        int2 = (uint32_t)(frame->pld.dat[7] << 24 | frame->pld.dat[6] << 16 | frame->pld.dat[5] << 8
                          | frame->pld.dat[4]);
        int3 = (uint32_t)(frame->pld.dat[11] << 24 | frame->pld.dat[10] << 16
                          | frame->pld.dat[9] << 8 | frame->pld.dat[8]);
        int4 = (uint32_t)(frame->pld.dat[15] << 24 | frame->pld.dat[14] << 16
                          | frame->pld.dat[13] << 8 | frame->pld.dat[12]);

        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %08x%08x%08x%08x...\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "FILE_TRANSFER_DATA",
               "FILE",
               int4,
               int3,
               int2,
               int1);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_print_textline
 *  Description:  Print one record line
 * =====================================================================================
 */
static void utracemon_print_textline(utframe_t *frame, char *appname, uint32_t count)
{
    char tbuf[30];
    uint32_t int1, int2, int3, int4;

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&frame->tms));

    if (frame->typ == utftxt) {
        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %s\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "TEXT",
               appname,
               (char *)&frame->pld.txt[0]);
    } else {
        int1 = (uint32_t)(frame->pld.dat[3] << 24 | frame->pld.dat[2] << 16 | frame->pld.dat[1] << 8
                          | frame->pld.dat[0]);
        int2 = (uint32_t)(frame->pld.dat[7] << 24 | frame->pld.dat[6] << 16 | frame->pld.dat[5] << 8
                          | frame->pld.dat[4]);
        int3 = (uint32_t)(frame->pld.dat[11] << 24 | frame->pld.dat[10] << 16
                          | frame->pld.dat[9] << 8 | frame->pld.dat[8]);
        int4 = (uint32_t)(frame->pld.dat[15] << 24 | frame->pld.dat[14] << 16
                          | frame->pld.dat[13] << 8 | frame->pld.dat[12]);

        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %08x%08x%08x%08x...\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "BINARY",
               appname,
               int4,
               int3,
               int2,
               int1);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_print_record
 *  Description:  Print one record line
 * =====================================================================================
 */
static void utracemon_print_record(utrecord_t *record)
{
    char tbuf[30];
    uint32_t int1, int2, int3;

    int1 = (uint32_t)(record->pkt.dat[3] << 24 | record->pkt.dat[2] << 16 | record->pkt.dat[1] << 8
                      | record->pkt.dat[0]);
    int2 = (uint32_t)(record->pkt.dat[7] << 24 | record->pkt.dat[6] << 16 | record->pkt.dat[5] << 8
                      | record->pkt.dat[4]);
    int3 = (uint32_t)(record->pkt.dat[11] << 24 | record->pkt.dat[10] << 16
                      | record->pkt.dat[9] << 8 | record->pkt.dat[8]);

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&record->pkt.tms));

    printf("[%08x][%20s ][%04x:%08x][%08x%08x%08x] %s -> %s\n",
           record->cnt,
           tbuf,
           record->pkt.aid,
           record->pkt.pid,
           int3,
           int2,
           int1,
           record->name,
           record->hline);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_connect_target
 *  Description:  Connect to target address
 * =====================================================================================
 */
static int utracemon_connect_target(utracemon_t *dec)
{
    struct sockaddr_in serv_addr;
    struct hostent *server;
    int port;

    dec->sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (dec->sfd < 0) {
        printf("utracemon: error opening socket\n");
        return (-1);
    }

    server = gethostbyname(dec->taddr);

    if (server == NULL) {
        printf("utracemon: error no such host\n");
        return (-1);
    }

    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(server->h_addr, (char *)&serv_addr.sin_addr.s_addr, (size_t)server->h_length);
    /* if a port option is available use it if not use default */
    port = dec->port > 0 ? dec->port : UT_UTRACED_PORT;
    serv_addr.sin_port = htons(port);

    if (connect(dec->sfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("utracemon: error connecting\n");
        return (-1);
    } else {
        dec->con = true;
    }

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracemon_terminate
 *  Description:  Terminate aplication by SIGINT
 * =====================================================================================
 */
static void utracemon_terminate(int signum)
{
    printf("utracemon: closing application with signal %d ... \n", signum);
    g_utracemon->con = false;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Main function for UTraceMon tool
 * =====================================================================================
 */
int main(int argc, char *argv[])
{
    int c;
    bool help = false;
    utracemon_t *decoder;
    int long_index = 0;

    /*	options	descriptor */
    struct option longopts[] = {{"list", required_argument, NULL, 'l'},
                                {"address", required_argument, NULL, 'a'},
                                {"port", required_argument, NULL, 'p'},
                                {"dump", required_argument, NULL, 'd'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    decoder = (utracemon_t *)malloc(sizeof(utracemon_t));
    if (utracemon_init(decoder) < 0) {
        exit(EXIT_FAILURE);
    }

    while ((c = getopt_long(argc, argv, "l:a:p:d:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'l':
            strncpy(decoder->dfn, optarg, UT_FILENAME_LEN);
            break;

        case 'a':
            strncpy(decoder->taddr, optarg, UT_FILENAME_LEN);
            break;

        case 'p':
            decoder->port = (int)atoi(optarg);
            break;

        case 'd':
            strncpy(decoder->ddn, optarg, UT_FILENAME_LEN);
            break;

        case 'h':
        default:
            help = true;
            break;
        }
    }

    g_utracemon = decoder;

    signal(SIGINT, utracemon_terminate);
    signal(SIGTERM, utracemon_terminate);
    signal(SIGPIPE, utracemon_catch_sigpipe);

    if ((strlen(decoder->dfn) == 0) && (strlen(decoder->taddr) == 0)) {
        utracemon_deinit(decoder);
        help = true;
    }

    if (strlen(decoder->ddn) >= 1) {
        if (transfer_set_path(&decoder->ft, decoder->ddn) < 0) {
            printf("utracemon: cannot create files in: %s\n", decoder->ddn);
            utracemon_deinit(decoder);
            help = true;
        }
    }

    if (help) {
        printf("UTraceMon %s: Decode trace data from utrace target\n\n", UTRACEVERSION);
        printf("Usage: utracemon --list <decoder.list> --address <hostname> --port <port>\n\n");
        printf("  Options:\n");
        printf("    --list, -l       <path>        UTrace decoders list file path\n");
        printf("    --address, -a    <hostname>    Target hostname or IP address for device to "
               "monitor\n");
        printf("    --dump, -d       <path>        Path for file transfer location\n\n");
        printf("    --port, -p       <port>        Target port number where utrace daemon is "
               "listening\n\n");
        printf("  Help:\n");
        printf("    --help, -h                     Print this help\n\n");

        exit(EXIT_SUCCESS);
    }

    if (utracemon_build_decoder(decoder) < 0) {
        printf("utracemon: cannot parse decoder list file %s\n", decoder->dfn);
    } else if (utracemon_decode_target(decoder) < 0) {
        printf("utracemon: cannot get data from target addr %s\n", decoder->taddr);
    } else {
        /* noop */
    }

    utracemon_deinit(decoder);
    free(decoder);
    printf("EXIT\n");

    exit(EXIT_SUCCESS);
}
