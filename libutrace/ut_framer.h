/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef UT_FRAMER_H
#define UT_FRAMER_H

#include "ut_types.h"
#include <pthread.h>

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct {
    utframe_t pkt; /* a frame of packets */
    utframe_t dat; /* a frame of row data */
    uint8_t cnt;
    pthread_mutex_t mtx;
    int (*complete_cb)(utframe_t *frame);
    void (*gettime)(uint64_t *ct);
} utframer_t;

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */
int ut_framer_init(utframer_t *framer, uint16_t aid, uint32_t pid);

int ut_framer_add_packet(utframer_t *framer, utpacket_t *packet);

int ut_framer_add_data(utframer_t *framer, void *data, uint8_t size);

int ut_framer_add_text(utframer_t *framer, const char *str);

int ut_framer_add_filename(utframer_t *framer, const char *str);

void ut_framer_set_complete_cb(utframer_t *framer, void *cb);

void ut_framer_set_gettime_cb(utframer_t *framer, void *cb);

int ut_framer_flush(utframer_t *framer);

#endif /* UT_FRAMER_H */
