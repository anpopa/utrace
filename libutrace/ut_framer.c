/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_framer.h"
#include "ut_types.h"
#include <string.h>

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static void ut_framer_clear_frame(utframer_t *framer);

static uint64_t ut_framer_gettime(utframer_t *framer);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_clear_frame
 *  Description:  Clear frame buffer
 * =====================================================================================
 */
static void ut_framer_clear_frame(utframer_t *framer)
{
    memset(&framer->pkt.pld.pks[0], 0, sizeof(utpacket_t) * UT_FRMPKT_CNT);
    framer->cnt = 0;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_init
 *  Description:  Init utframer_t structure
 * =====================================================================================
 */
int ut_framer_init(utframer_t *framer, uint16_t aid, uint32_t pid)
{
    memset(&framer->pkt, 0, sizeof(utframe_t));
    memset(&framer->dat, 0, sizeof(utframe_t));
    framer->complete_cb = NULL;
    framer->gettime = NULL;

    /* Init framer struct */
    framer->pkt.sof = UTSOF;
    framer->pkt.aid = aid;
    framer->pkt.pid = pid;
    framer->pkt.typ = utfpkt;
    framer->cnt = 0;

    /* Init dat struct */
    framer->dat.sof = UTSOF;
    framer->dat.aid = aid;
    framer->dat.pid = pid;

    pthread_mutex_init(&framer->mtx, NULL);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_add_packet
 *  Description:  Add one packet to frame and flush when frame complete
 * =====================================================================================
 */
int ut_framer_add_packet(utframer_t *framer, utpacket_t *packet)
{
    int status = 0;

    pthread_mutex_lock(&framer->mtx);
    framer->pkt.pld.pks[framer->cnt++] = *packet;
    if (framer->cnt >= UT_FRMPKT_CNT) {
        framer->pkt.tms = (uint64_t)ut_framer_gettime(framer);
        status = ut_framer_flush(framer);
    }
    pthread_mutex_unlock(&framer->mtx);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_add_data
 *  Description:  Add data to frame and flush framedat
 * =====================================================================================
 */
int ut_framer_add_data(utframer_t *framer, void *data, uint8_t size)
{
    int status = 0;

    size = (uint8_t)(size < UT_FRMDAT_SZ ? size : UT_FRMDAT_SZ);

    pthread_mutex_lock(&framer->mtx);
    memcpy(&framer->dat.pld.dat[0], data, size);
    framer->dat.typ = utfbin;
    framer->dat.tms = (uint64_t)ut_framer_gettime(framer);
    framer->complete_cb(&framer->dat);
    memset(&framer->dat.pld.txt[0], 0, UT_FRMDAT_SZ);
    pthread_mutex_unlock(&framer->mtx);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_add_text
 *  Description:  Add string to frame and flush
 * =====================================================================================
 */
int ut_framer_add_text(utframer_t *framer, const char *str)
{
    int status = 0;

    pthread_mutex_lock(&framer->mtx);
    strncpy((char *)&framer->dat.pld.txt, str, UT_FRMDAT_SZ);
    framer->dat.typ = utftxt;
    framer->dat.tms = (uint64_t)ut_framer_gettime(framer);
    framer->complete_cb(&framer->dat);
    memset(&framer->dat.pld.txt[0], 0, UT_FRMDAT_SZ);
    pthread_mutex_unlock(&framer->mtx);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_add_filename
 *  Description:  Add filname to frame type fls and flush
 * =====================================================================================
 */
int ut_framer_add_filename(utframer_t *framer, const char *str)
{
    int status = 0;

    pthread_mutex_lock(&framer->mtx);
    strncpy((char *)&framer->dat.pld.txt, str, UT_FRMDAT_SZ);
    framer->dat.typ = utffrq;
    framer->dat.tms = (uint64_t)ut_framer_gettime(framer);
    framer->complete_cb(&framer->dat);
    memset(&framer->dat.pld.txt[0], 0, UT_FRMDAT_SZ);
    pthread_mutex_unlock(&framer->mtx);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_flush
 *  Description:  Flush frame by calling complete callback. Clear frame for next cycle
 * =====================================================================================
 */
int ut_framer_flush(utframer_t *framer)
{
    int status = -1;

    if (framer->cnt > 0) {
        framer->pkt.tms = (uint64_t)ut_framer_gettime(framer);
        status = framer->complete_cb(&framer->pkt);
        ut_framer_clear_frame(framer);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_set_gettime_cb
 *  Description:  Gettime callback to timestamp frame
 * =====================================================================================
 */
void ut_framer_set_gettime_cb(utframer_t *framer, void *cb)
{
    framer->gettime = cb;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_set_complete_cb
 *  Description:  Set function to call when frame should be flushed to pipe
 * =====================================================================================
 */
void ut_framer_set_complete_cb(utframer_t *framer, void *cb)
{
    framer->complete_cb = cb;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_framer_gettime
 *  Description:  Get system time
 * =====================================================================================
 */
uint64_t ut_framer_gettime(utframer_t *framer)
{
    uint64_t ct = 0;

    if (framer->gettime) {
        framer->gettime(&ct);
    }

    return (ct);
}
