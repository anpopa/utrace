/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_single_app.h"
#include <stdlib.h>

utrace_t *utrace_global_instance = NULL;

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_init
 *  Description:  Initialize UTrace system for this process.
 *                This should be done before use any other utrace_* API
 * =====================================================================================
 */
int utrace_init(uint16_t appid, uint32_t pid)
{
    utrace_global_instance = (utrace_t *)malloc(sizeof(utrace_t));

    if (utrace_global_instance) {
        return (ut_client_init(utrace_global_instance, appid, pid));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_free
 *  Description:  Free utrace data
 * =====================================================================================
 */
int utrace_free(void)
{
    if (utrace_global_instance) {
        free(utrace_global_instance);
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_open
 *  Description:  Connect to utraced. When connected UTrace data packets will be sent to
 *                utraced accumulator
 * =====================================================================================
 */
int utrace_open(const char *name)
{
    if (utrace_global_instance) {
        return (ut_client_open(utrace_global_instance, name));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_close
 *  Description:  Disconnect from utraced
 * =====================================================================================
 */
int utrace_close(void)
{
    if (utrace_global_instance) {
        return (ut_client_close(utrace_global_instance));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_flush
 *  Description:  Flush the buffers
 * =====================================================================================
 */
void utrace_flush(void)
{
    if (utrace_global_instance) {
        ut_client_flush(utrace_global_instance);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_settime_cb
 *  Description:  Set callback to get system time for frame timestamp
 * =====================================================================================
 */
void utrace_settime_cb(void *cb)
{
    ut_client_settime_cb(utrace_global_instance, cb);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_add_info
 *  Description:  Add one entry with 'info' priority
 * =====================================================================================
 */
int utrace_add_info(uint16_t hash, void *data, uint8_t size, bool tms)
{
    if (utrace_global_instance) {
        return (ut_client_add(utrace_global_instance, hash, data, size, uinfo, tms));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_add_error
 *  Description:  Add one entry with 'error' priority
 * =====================================================================================
 */
int utrace_add_error(uint16_t hash, void *data, uint8_t size, bool tms)
{
    if (utrace_global_instance) {
        return (ut_client_add(utrace_global_instance, hash, data, size, uerror, tms));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_dump
 *  Description:  Dump raw data to utrace
 * =====================================================================================
 */
int utrace_dump(void *data, uint8_t size)
{
    if (utrace_global_instance) {
        return (ut_client_dump(utrace_global_instance, data, size));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_print
 *  Description:  Print string to utrace
 * =====================================================================================
 */
int utrace_print(const char *str)
{
    if (utrace_global_instance) {
        return (ut_client_print(utrace_global_instance, str));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_dump_file
 *  Description:  Request utraced to dump file
 * =====================================================================================
 */
int utrace_dump_file(const char *str)
{
    if (utrace_global_instance) {
        return (ut_client_dump_file(utrace_global_instance, str));
    }

    return (-1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utrace_trap
 *  Description:  Trace received data as error and force process to stop with error
 * =====================================================================================
 */
void utrace_trap(void *data, uint8_t size)
{
    volatile int *fault = NULL;
    int status;

    if (utrace_global_instance) {
        ut_client_add(utrace_global_instance, UT_HASH_TRAP, data, size, uerror, true);
    }

    status = *fault;
}
