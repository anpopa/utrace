/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef UT_ENTRY_LIST_H
#define UT_ENTRY_LIST_H

#include "sys/queue.h"
#include "ut_types.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct utracellel {
    utentry_t dat;
    SLIST_ENTRY(utracellel) entries;
} utracellel_t;

typedef struct utentryl {
    pthread_mutex_t mtx;
    SLIST_HEAD(utentrylist, utracellel) head;
} utentryl_t;

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */
int ut_entry_list_init(utentryl_t *l);

int ut_entry_list_delete(utentryl_t *l);

int ut_entry_list_add(utentryl_t *l, utentry_t *d);

int ut_entry_list_rem(utentryl_t *l, utentry_t *d);

int ut_entry_list_find(utentryl_t *l, uint16_t appid, uint16_t hash, utentry_t *entry);

int ut_entry_list_appname(utentryl_t *l, uint16_t appid, char *name);

uint32_t ut_entry_list_size(utentryl_t *l);

#endif /* UT_ENTRY_LIST_H */
