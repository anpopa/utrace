/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef UT_CONFIG_H
#define UT_CONFIG_H

/* utracesys version string  */
#ifndef UTRACEVERSION
#define UTRACEVERSION "v0.1"
#endif

/* utpacket_t sizes */
#ifndef UT_PKTHEAD_SZ
#define UT_PKTHEAD_SZ 4 /* 2 bytes hash + 2 bites timestamp */
#endif

#ifndef UT_PKTPLD_SZ
#define UT_PKTPLD_SZ 12 /* number of bytes for payload */
#endif

#ifndef UT_PKT_SZ
#define UT_PKT_SZ UT_PKTHEAD_SZ + UT_PKTPLD_SZ /* this should always be 16! */
#endif

/* utframe_t sizes */
#ifndef UT_FRMHEAD_SZ
#define UT_FRMHEAD_SZ 16 /* 1SOF+2AID+4PID+8TMS+1TYP */
#endif

#ifndef UT_FRMPKT_CNT
#define UT_FRMPKT_CNT                                                                      \
    15 /* number of packets per frame, this can be calculated as: FRAME SIZE / UT_PKT_SZ - \
          UT_FRMHEAD_SZ */
#endif

#ifndef UT_FRMDAT_SZ
#define UT_FRMDAT_SZ UT_FRMPKT_CNT *(UT_PKT_SZ) /* used by txt and bin payload */
#endif

#ifndef UT_FRM_SZ
#define UT_FRM_SZ UT_FRMHEAD_SZ + UT_FRMDAT_SZ /* always multiple of 16! */
#endif

/* SOF identifier  */
#ifndef UTSOF
#define UTSOF 0x23
#endif

/* Constant string lenghts */
#ifndef UT_APPNAME_LEN
#define UT_APPNAME_LEN 32
#endif

#ifndef UT_HEADLINE_LEN
#define UT_HEADLINE_LEN 110
#endif

#ifndef UT_DESC_LEN
#define UT_DESC_LEN 110
#endif

#ifndef UT_FILENAME_LEN
#define UT_FILENAME_LEN 255
#endif

/* utrace ID */
#ifndef UTRACEID
#define UTRACEID 0xFFFD
#endif

/* utracedem ID */
#ifndef UTRACEDID
#define UTRACEDID 0xFFFE
#endif

/* Hash ID reserved for trap signaling */
#ifndef UT_HASH_TRAP
#define UT_HASH_TRAP 0xFFFE
#endif

/* Used file paths */
#ifndef UT_PIPE_PTH
#define UT_PIPE_PTH "/tmp/.utrace.pipe"
#endif

#ifndef UT_DUMPFILE_PTH
#define UT_DUMPFILE_PTH "utracedump.bin"
#endif

/* utracedem listen port */
#ifndef UT_UTRACED_PORT
#define UT_UTRACED_PORT 5234
#endif

#endif /* UT_CONFIG_H */
