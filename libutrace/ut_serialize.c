/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_serialize.h"
#include "ut_types.h"

int ut_serialize_uint8_encode(uint8_t dat, uint8_t *output, int *counter)
{
    int cnt = -1;

    *output = (uint8_t)dat;
    if (counter) {
        cnt = (*counter)++;
    }

    return (cnt);
}

int ut_serialize_uint16_encode(uint16_t dat, uint8_t *output, int *counter)
{
    int size = 0;

    output[size++] = (uint8_t)(dat >> 8);
    output[size++] = (uint8_t)dat;
    if (counter) {
        *counter += size;
    }

    return (size);
}

int ut_serialize_uint32_encode(uint32_t dat, uint8_t *output, int *counter)
{
    int size = 0;

    output[size++] = (uint8_t)(dat >> 24);
    output[size++] = (uint8_t)(dat >> 16);
    output[size++] = (uint8_t)(dat >> 8);
    output[size++] = (uint8_t)dat;
    if (counter) {
        *counter += size;
    }

    return (size);
}

int ut_serialize_uint64_encode(uint64_t dat, uint8_t *output, int *counter)
{
    int size = 0;

    output[size++] = (uint8_t)(dat >> 56);
    output[size++] = (uint8_t)(dat >> 48);
    output[size++] = (uint8_t)(dat >> 40);
    output[size++] = (uint8_t)(dat >> 32);
    output[size++] = (uint8_t)(dat >> 24);
    output[size++] = (uint8_t)(dat >> 16);
    output[size++] = (uint8_t)(dat >> 8);
    output[size++] = (uint8_t)dat;

    if (counter) {
        *counter += size;
    }

    return (size);
}

uint8_t ut_serialize_uint8_decode(uint8_t *buf, int *counter)
{
    if (counter) {
        (*counter)++;
    }

    return ((uint8_t)*buf);
}

uint16_t ut_serialize_uint16_decode(uint8_t *buf, int *counter)
{
    if (counter) {
        *counter += 2;
    }

    return ((uint16_t)((uint16_t)(buf[0] << 8) | (uint16_t)buf[1]));
}

uint32_t ut_serialize_uint32_decode(uint8_t *buf, int *counter)
{
    if (counter) {
        *counter += 4;
    }

    return ((uint32_t)((uint32_t)buf[0] << 24 | (uint32_t)buf[1] << 16 | (uint32_t)buf[2] << 8
                       | (uint32_t)buf[3]));
}

uint64_t ut_serialize_uint64_decode(uint8_t *buf, int *counter)
{
    if (counter) {
        *counter += 8;
    }

    return ((uint64_t)((uint64_t)buf[0] << 56 | (uint64_t)buf[1] << 48 | (uint64_t)buf[2] << 40
                       | (uint64_t)buf[3] << 32 | (uint64_t)buf[4] << 24 | (uint64_t)buf[5] << 16
                       | (uint64_t)buf[6] << 8 | (uint64_t)buf[7]));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_serialize_frame_encode
 *  Description:  Encode one frame
 * =====================================================================================
 */
int ut_serialize_frame_encode(utframe_t *frame, uint8_t *output)
{
    int i, j;
    int size = 0;

    ut_serialize_uint8_encode(frame->sof, &output[size], &size);
    ut_serialize_uint8_encode(frame->typ, &output[size], &size);
    ut_serialize_uint16_encode(frame->aid, &output[size], &size);
    ut_serialize_uint32_encode(frame->pid, &output[size], &size);
    ut_serialize_uint64_encode(frame->tms, &output[size], &size);

    switch (frame->typ) {
    case utfpkt:
        for (i = 0; i < UT_FRMPKT_CNT; i++) {
            ut_serialize_uint16_encode(frame->pld.pks[i].hsh, &output[size], &size);
            ut_serialize_uint16_encode(frame->pld.pks[i].tms, &output[size], &size);

            for (j = 0; j < UT_PKTPLD_SZ; j++) {
                ut_serialize_uint8_encode(frame->pld.pks[i].dat[j], &output[size], &size);
            }
        }
        break;

    case utffst:
    case utffso:
    case utffpl:
    case utfbin:
        for (i = 0; i < UT_FRMDAT_SZ; i++) {
            ut_serialize_uint8_encode(frame->pld.dat[i], &output[size], &size);
        }
        break;

    case utftxt:
        for (i = 0; i < UT_FRMDAT_SZ; i++) {
            output[size++] = (uint8_t)frame->pld.dat[i];
        }
        break;

    default:
        break;
    }

    return (size);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_serialize_frame_decode
 *  Description:  Decode one frame
 * =====================================================================================
 */
int ut_serialize_frame_decode(uint8_t *input, utframe_t *frame)
{
    int i, j;
    int size = 0;

    frame->sof = ut_serialize_uint8_decode(&input[size], &size);
    frame->typ = ut_serialize_uint8_decode(&input[size], &size);
    frame->aid = ut_serialize_uint16_decode(&input[size], &size);
    frame->pid = ut_serialize_uint32_decode(&input[size], &size);
    frame->tms = ut_serialize_uint64_decode(&input[size], &size);

    switch (frame->typ) {
    case utfpkt:
        for (i = 0; i < UT_FRMPKT_CNT; i++) {
            frame->pld.pks[i].hsh = ut_serialize_uint16_decode(&input[size], &size);
            frame->pld.pks[i].tms = ut_serialize_uint16_decode(&input[size], &size);
            for (j = 0; j < UT_PKTPLD_SZ; j++) {
                frame->pld.pks[i].dat[j] = ut_serialize_uint8_decode(&input[size], &size);
            }
        }
        break;

    case utffst:
    case utffso:
    case utffpl:
    case utfbin:
        for (i = 0; i < UT_FRMDAT_SZ; i++) {
            frame->pld.dat[i] = ut_serialize_uint8_decode(&input[size], &size);
        }
        break;

    case utftxt:
        for (i = 0; i < UT_FRMDAT_SZ; i++) {
            frame->pld.dat[i] = input[size++];
        }
        break;

    default:
        break;
    }

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_serialize_entry_encode
 *  Description:  Encode one entry
 * =====================================================================================
 */
int ut_serialize_entry_encode(utentry_t *entry, uint8_t *output)
{
    int i;
    int size = 0;

    ut_serialize_uint16_encode(entry->hsh, &output[size], &size);
    ut_serialize_uint16_encode(entry->aid, &output[size], &size);

    for (i = 0; i < UT_HEADLINE_LEN; i++) {
        output[size++] = (uint8_t)entry->hline[i];
    }

    for (i = 0; i < UT_DESC_LEN; i++) {
        output[size++] = (uint8_t)entry->desc[i];
    }

    for (i = 0; i < UT_APPNAME_LEN; i++) {
        output[size++] = (uint8_t)entry->name[i];
    }

    return (size);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_serialize_entry_decode
 *  Description:  Decode one entry
 * =====================================================================================
 */
int ut_serialize_entry_decode(uint8_t *input, utentry_t *entry)
{
    int i;
    int size = 0;

    entry->hsh = ut_serialize_uint16_decode(&input[size], &size);
    entry->aid = ut_serialize_uint16_decode(&input[size], &size);

    for (i = 0; i < UT_HEADLINE_LEN; i++) {
        entry->hline[i] = input[size++];
    }

    for (i = 0; i < UT_DESC_LEN; i++) {
        entry->desc[i] = input[size++];
    }

    for (i = 0; i < UT_APPNAME_LEN; i++) {
        entry->name[i] = input[size++];
    }

    return (size);
}
