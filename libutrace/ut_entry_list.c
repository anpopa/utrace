/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_entry_list.h"

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_init
 *  Description:  Initialize entry list
 * =====================================================================================
 */
int ut_entry_list_init(utentryl_t *l)
{
    SLIST_INIT(&l->head);
    pthread_mutex_init(&l->mtx, NULL);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_delete
 *  Description:  Delete list
 * =====================================================================================
 */
int ut_entry_list_delete(utentryl_t *l)
{
    while (!SLIST_EMPTY(&l->head)) {
        utracellel_t *elem;

        elem = SLIST_FIRST(&l->head);
        SLIST_REMOVE_HEAD(&l->head, entries);
        free(elem);
    }

    pthread_mutex_destroy(&l->mtx);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_size
 *  Description:  Get list size
 * =====================================================================================
 */
uint32_t ut_entry_list_size(utentryl_t *l)
{
    uint32_t count = 0;
    utracellel_t *elem;

    SLIST_FOREACH(elem, &l->head, entries)
    {
        count++;
    }

    return (count);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_add
 *  Description:  Add one entry to list
 * =====================================================================================
 */
int ut_entry_list_add(utentryl_t *l, utentry_t *d)
{
    utracellel_t *nelem = (utracellel_t *)malloc(sizeof(utracellel_t));
    int status = 0;

    if (nelem) {
        memcpy(&(nelem->dat), d, sizeof(utentry_t));
        pthread_mutex_lock(&l->mtx);
        SLIST_INSERT_HEAD(&l->head, nelem, entries);
        pthread_mutex_unlock(&l->mtx);
    } else {
        status = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_rem
 *  Description:  Remove one element from list
 * =====================================================================================
 */
int ut_entry_list_rem(utentryl_t *l, utentry_t *d)
{
    int status = -1;
    utracellel_t *elem;

    SLIST_FOREACH(elem, &l->head, entries)
    {
        if ((d->hsh == elem->dat.hsh) && (d->aid == elem->dat.aid)) {
            pthread_mutex_lock(&l->mtx);
            SLIST_REMOVE(&l->head, elem, utracellel, entries);
            pthread_mutex_unlock(&l->mtx);
            free(elem);
            status = 0;
        }
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_find
 *  Description:  Find element in list buffer maching appid and hash
 * =====================================================================================
 */
int ut_entry_list_find(utentryl_t *l, uint16_t appid, uint16_t hash, utentry_t *entry)
{
    int status = -1;
    utracellel_t *elem;

    SLIST_FOREACH(elem, &l->head, entries)
    {
        if ((elem->dat.aid == appid) && (elem->dat.hsh == hash)) {
            memcpy(entry, &(elem->dat), sizeof(utentry_t));
            status = 0;
        }
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_entry_list_appname
 *  Description:  Get name string for appid if available
 * =====================================================================================
 */
int ut_entry_list_appname(utentryl_t *l, uint16_t appid, char *name)
{
    int status = 0;
    utracellel_t *elem;

    SLIST_FOREACH(elem, &l->head, entries)
    {
        if (appid == elem->dat.aid) {
            strncpy(name, &elem->dat.name[0], UT_APPNAME_LEN);
            status = 1;
        }
    }

    return (status);
}
