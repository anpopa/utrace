/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef UT_TYPES_H
#define UT_TYPES_H

#include "ut_config.h"
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef enum { uinfo, uerror } utlevel_t;

typedef enum {
    utfpkt,
    utftxt,
    utfbin,
    utffrq, /* request to utraced for filetransfer */
    utffst, /* file transfer begin marker frame */
    utffso, /* file transfer end marker frame */
    utffpl  /* file transfer payload frame */
} utframetype_t;

/* We force byte alignment to avoid any pedding by compiler
 * in this way a frame will always be 128 bytes in memory */
#pragma pack(push, 1)
typedef struct {
    uint16_t hsh;
    uint16_t tms;
    uint8_t dat[UT_PKTPLD_SZ];
} utpacket_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union {
    utpacket_t pks[UT_FRMPKT_CNT];
    uint8_t dat[UT_FRMDAT_SZ];
    uint8_t txt[UT_FRMDAT_SZ];
} utpayload_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
    uint8_t sof;
    uint8_t typ;
    uint16_t aid;
    uint32_t pid;
    uint64_t tms;
    utpayload_t pld;
} utframe_t;
#pragma pack(pop)

typedef struct {
    uint16_t aid;
    uint16_t hsh;
    uint32_t pid;
    uint64_t tms;
    uint8_t dat[UT_PKTPLD_SZ];
} utpacketinfo_t;

/* For changes in utentry_t structure modules
 * like utracegen should be updated! See utracegen/include/utgen.h */
#pragma pack(push, 1)
typedef struct {
    uint16_t hsh;
    uint16_t aid;
    char hline[UT_HEADLINE_LEN];
    char desc[UT_DESC_LEN];
    char name[UT_APPNAME_LEN];
} utentry_t;
#pragma pack(pop)

typedef struct {
    utpacketinfo_t pkt;
    uint32_t cnt;
    char name[UT_APPNAME_LEN];
    char hline[UT_HEADLINE_LEN];
    char desc[UT_DESC_LEN];
} utrecord_t;
#endif /* UT_TYPES_H */
