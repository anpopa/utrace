/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_compress.h"

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_compress_string_inflate
 *  Description:  Inflate source string and write output to dest string
 * =====================================================================================
 */
int ut_compress_string_inflate(uint8_t *dst, uint8_t *src, uint32_t ssz)
{
    int i, cnt, dsz = 0;

    for (i = 0; i < ssz; i++) {
        if (src[i] != src[i + 1]) {
            dst[dsz++] = src[i];
        } else {
            /* At least two consecutive bytes repeating.
             * Write this byte two times to output followed by the number of repetition */
            dst[dsz++] = src[i];
            dst[dsz++] = src[i];
            for (cnt = 1; (src[i] == src[i + 1]) && (i < ssz); cnt++, i++) {
                /* noop */
            }
            dst[dsz++] = (uint8_t)cnt;
        }
    }

    return (dsz);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_compress_string_deflate
 *  Description:  Deflate source string and write output to dest string
 * =====================================================================================
 */
int ut_compress_string_deflate(uint8_t *dst, uint8_t *src, uint32_t ssz)
{
    int i, cnt, dsz = 0;

    for (i = 0; i < ssz; i++) {
        if (src[i] != src[i + 1]) {
            dst[dsz++] = src[i];
        } else {
            /* This byte is repeting. Write it to output for number of repetition */
            for (cnt = 0; cnt < src[i + 2]; cnt++) {
                dst[dsz++] = src[i];
            }

            i += 2;
        }
    }

    return (dsz);
}
