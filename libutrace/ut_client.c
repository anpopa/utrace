/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_client.h"
#include <string.h>
#include <sys/fcntl.h>

static utrace_t *g_trace;

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static uint64_t ut_client_gettime(utrace_t *trace);

static int ut_client_frame_complete(utframe_t *frame);

static void ut_client_frame_timestamp(uint64_t *ct);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_init
 *  Description:  Initialize UTrace instance
 * =====================================================================================
 */
int ut_client_init(utrace_t *trace, uint16_t appid, uint32_t pid)
{
    int status = -1;

    if (trace) {
        g_trace = trace;
        trace->pipe = -1;
        trace->gettime = NULL;
        ut_framer_init(&trace->mng, appid, pid);
        ut_framer_set_complete_cb(&trace->mng, &ut_client_frame_complete);
        ut_framer_set_gettime_cb(&trace->mng, &ut_client_frame_timestamp);

        status = 0;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_open
 *  Description:  Open UNIX pipe connection with utraced
 * =====================================================================================
 */
int ut_client_open(utrace_t *trace, const char *path)
{
    int status = -1;

    if (trace->pipe < 0) {
        trace->pipe = open(path, O_WRONLY);
        if (trace->pipe > 0) {
            status = 0;
        }
    } else {
        status = 1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_close
 *  Description:  close UTrace connection
 * =====================================================================================
 */
int ut_client_close(utrace_t *trace)
{
    if (trace->pipe >= 0) {
        ut_framer_flush(&trace->mng);
        close(trace->pipe);
        trace->pipe = -1;
    }

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_flush
 *  Description:  Flush utrace buffers
 * =====================================================================================
 */
void ut_client_flush(utrace_t *trace)
{
    if (trace->pipe >= 0) {
        ut_framer_flush(&trace->mng);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_settime_cb
 *  Description:  Set current time callback to be used for entry time stamp
 * =====================================================================================
 */
void ut_client_settime_cb(utrace_t *trace, void *cb)
{
    trace->gettime = cb;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_add
 *  Description:  Main print function to be used for adding trace entries
 * =====================================================================================
 */
int ut_client_add(utrace_t *trace, uint16_t hash, void *data, uint8_t size, utlevel_t lvl, bool tms)
{
    utpacket_t packet;
    int status;

    if (trace->pipe < 0) {
        return (-1);
    }

    memset(&packet, 0, sizeof(utpacket_t));

    packet.hsh = hash;

    if (tms) {
        packet.tms = (uint16_t)ut_client_gettime(trace);
    }

    if (data && (size > 0)) {
        size = (uint8_t)(size < sizeof(packet.dat) ? size : sizeof(packet.dat));
        memcpy(&packet.dat, data, size);
    }

    status = ut_framer_add_packet(&trace->mng, &packet);
    if ((status == 0) && (lvl == uerror)) {
        status = ut_framer_flush(&trace->mng);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_dump
 *  Description:  Main dump data function to send raw data
 * =====================================================================================
 */
int ut_client_dump(utrace_t *trace, void *data, uint8_t size)
{
    return (ut_framer_add_data(&trace->mng, data, size));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_print
 *  Description:  Main print function to send one string
 * =====================================================================================
 */
int ut_client_print(utrace_t *trace, const char *str)
{
    return (ut_framer_add_text(&trace->mng, str));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_dump_file
 *  Description:  Send one string with frame type utfls
 * =====================================================================================
 */
int ut_client_dump_file(utrace_t *trace, const char *str)
{
    return (ut_framer_add_filename(&trace->mng, str));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_gettime
 *  Description:  Call gettime callback if registered
 * =====================================================================================
 */
static uint64_t ut_client_gettime(utrace_t *trace)
{
    uint64_t ct = 0;

    if (trace->gettime) {
        trace->gettime(&ct);
    }

    return (ct);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_frame_complete
 *  Description:  Callback funtion ut_framer will call to flush a complete frame
 * =====================================================================================
 */
static int ut_client_frame_complete(utframe_t *frame)
{
    int status;

    status = (int)write(g_trace->pipe, frame, sizeof(utframe_t));
    if ((status < 0) && (g_trace->pipe > 0)) {
        close(g_trace->pipe);
        g_trace->pipe = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_client_frame_timestamp
 *  Description:  Callback funtion for ut_framer to timestamp frames
 * =====================================================================================
 */
static void ut_client_frame_timestamp(uint64_t *ct)
{
    *ct = ut_client_gettime(g_trace);
}
