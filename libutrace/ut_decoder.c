/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_decoder.h"
#include "ut_serialize.h"
#include "ut_types.h"
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <unistd.h>

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static int ut_decoder_read_entry(utentry_t *entry, int fd);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_init
 *  Description:  Init decoder instance
 * =====================================================================================
 */
int ut_decoder_init(utracedecoder_t *decoder)
{
    int status = -1;

    if (ut_entry_list_init(&(decoder->list)) >= 0) {
        decoder->isinit = true;
        decoder->count = 0;
        status = 0;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_free
 *  Description:  Free decoder instance
 * =====================================================================================
 */
int ut_decoder_free(utracedecoder_t *decoder)
{
    int status = -1;

    if (decoder->isinit) {
        if (ut_entry_list_delete(&(decoder->list)) >= 0) {
            decoder->count = 0;
            decoder->isinit = false;
            status = 0;
        }
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_clear
 *  Description:  Clear decoder records
 * =====================================================================================
 */
int ut_decoder_clear(utracedecoder_t *decoder)
{
    int status = -1;

    if (decoder->isinit) {
        if (ut_entry_list_delete(&(decoder->list)) >= 0) {
            decoder->count = 0;
            status = 0;
        }
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_add_file
 *  Description:  Add one file to parse for decoders
 * =====================================================================================
 */
int ut_decoder_add_file(utracedecoder_t *decoder, const char *fname)
{
    utentry_t *entry;
    int status = 0;
    int fd;

    fd = open(fname, O_RDONLY);
    if (fd < 0) {
        return (-1);
    }

    entry = (utentry_t *)malloc(sizeof(utentry_t));
    if (!entry) {
        close(fd);
        return (-1);
    }

    if (decoder->isinit) {
        do {
            memset(entry, 0, sizeof(utentry_t));
            status = ut_decoder_read_entry(entry, fd);
            if (status >= 0) {
                (void)ut_entry_list_add(&(decoder->list), entry);
            }
        } while (status >= 0);
        status = 0;
    }

    free(entry);
    close(fd);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_read_entry
 *  Description:  Read one entry from file and decode
 * =====================================================================================
 */
static int ut_decoder_read_entry(utentry_t *entry, int fd)
{
    int status = 0;
    unsigned char sentry[sizeof(utentry_t)];

    memset(sentry, 0, sizeof(utentry_t));

    status = (uint32_t)read(fd, sentry, sizeof(utentry_t));
    if (status > 0) {
        ut_serialize_entry_decode(sentry, entry);
    } else {
        status = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_apply
 *  Description:  Apply decoder to one frame
 * =====================================================================================
 */
int ut_decoder_apply(utracedecoder_t *decoder, utpacketinfo_t *pkt, utrecord_t *record)
{
    int status = 0;
    utentry_t *entry;

    entry = (utentry_t *)malloc(sizeof(utentry_t));
    if (!entry) {
        return (-1);
    }

    memset(entry, 0, sizeof(utentry_t));
    memset(record, 0, sizeof(utrecord_t));
    status = ut_entry_list_find(&(decoder->list), pkt->aid, pkt->hsh, entry);

    if (status >= 0) {
        strncpy(record->name, entry->name, strlen(entry->name));
        strncpy(record->hline, entry->hline, strlen(entry->hline));
        strncpy(record->desc, entry->desc, strlen(entry->desc));
    } else {
        if (pkt->aid == UTRACEID) {
            sprintf(record->name, "utrace");
            sprintf(record->hline, "ID %04x", pkt->hsh);
        } else {
            sprintf(record->name, "%d", pkt->aid);
            sprintf(record->hline, "Unknown hash = %04x", pkt->hsh);
        }
    }

    memcpy(&record->pkt, pkt, sizeof(utpacketinfo_t));
    free(entry);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  ut_decoder_get_appname
 *  Description:  Get appname if available in decoder
 * =====================================================================================
 */
int ut_decoder_get_appname(utracedecoder_t *decoder, uint16_t appid, char *name)
{
    return (ut_entry_list_appname(&decoder->list, appid, name));
}
