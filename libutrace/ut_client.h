/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef UT_CLIENT_H
#define UT_CLIENT_H

#include "ut_framer.h"
#include "ut_types.h"
#include <stdio.h>
#include <unistd.h>

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct utrace {
    int pipe;
    utframer_t mng;
    void (*gettime)(uint64_t *ct);
} utrace_t;

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */
int ut_client_init(utrace_t *trace, uint16_t appid, uint32_t pid);

int ut_client_open(utrace_t *trace, const char *path);

int ut_client_close(utrace_t *trace);

void ut_client_settime_cb(utrace_t *trace, void *cb);

int ut_client_add(utrace_t *trace,
                  uint16_t hash,
                  void *data,
                  uint8_t size,
                  utlevel_t level,
                  bool tms);

int ut_client_dump(utrace_t *trace, void *data, uint8_t size);

int ut_client_print(utrace_t *trace, const char *str);

int ut_client_dump_file(utrace_t *trace, const char *str);

void ut_client_flush(utrace_t *trace);

#endif /* UT_CLIENT_H */
