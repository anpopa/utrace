/*
 * =====================================================================================
 *  The UTSYSLOG Copyright
 *
 *  Copyright (c) 2017, Alin Popa (alin.popa@fxdata.ro)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include <getopt.h>
#include <memory.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/event.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#include "ut_client_app.h"
#include "utracedefs.h"

#ifndef UTSYSLOGVER
#define UTSYSLOGVER "v0.1"
#endif

#define UTSYSLOG_PIDFILE_PATH "/var/run/utsyslog.pid"

static bool g_running = false;

static int utsyslog_run(const char *fname);
static void utsyslog_terminate(int signum);

static inline void systime(uint64_t *ct)
{
    *ct = (uint64_t)time(NULL);
}

static int utsyslog_doread(int fd);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utsyslog_terminate
 *  Description:  SIGINT handler to terminate application
 * =====================================================================================
 */
static void utsyslog_terminate(int signum)
{
    g_running = false;
    utrace_add_info(UTSYSLOG_TERMINATE, NULL, 0, true);
    utrace_close();
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utsyslog_daemonize
 *  Description:  UTSYSLOG daemonize
 * =====================================================================================
 */
static void utsyslog_daemonize(void)
{
    pid_t pid, sid;
    int fd;

    /* already a daemon */
    if (getppid() == 1) {
        return;
    }

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS); /* Killing the Parent Process */
    }

    /* At this point we are executing as the child process */

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory. */
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    fd = open("/dev/null", O_RDWR, 0);
    if (fd != -1) {
        dup2(fd, STDIN_FILENO);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);

        if (fd > 2) {
            close(fd);
        }
    }

    /* resettign File Creation Mask */
    umask(027);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utsyslog_create_pidfile
 *  Description:  UTSYSLOG create the pidfile
 * =====================================================================================
 */
static int utsyslog_create_pidfile(void)
{
    int status = 0;
    FILE *file = NULL;
    int pid = -1;

    file = fopen(UTSYSLOG_PIDFILE_PATH, "w");
    if (file == NULL) {
        status = -1;
    }

    if (status >= 0) {
        pid = getpid();
        if (!fprintf(file, "%d\n", pid)) {
            status = -1;
        }

        fclose(file);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utsyslog_run
 *  Description:  UTSYSLOG run the read loop for syslog
 * =====================================================================================
 */
static int utsyslog_run(const char *fname)
{
    struct kevent event;  /* Event we want to monitor */
    struct kevent tevent; /* Event triggered */
    struct timespec timeout;

    int kq, fd;
    int ret = -1;

    fd = open(fname, O_RDONLY | O_NONBLOCK);
    if (fd < 0) {
        goto exit;
    }

    /* Create kqueue. */
    kq = kqueue();
    if (kq == -1) {
        perror("kqueue() failed");
        goto exit;
    }

    /* Initialize kevent structure. */
    EV_SET(&event, fd, EVFILT_VNODE, EV_ADD | EV_CLEAR, NOTE_WRITE, 0, NULL);

    /* Attach event to the	kqueue.	*/
    ret = kevent(kq, &event, 1, NULL, 0, NULL);
    if ((ret < 0) || (event.flags & EV_ERROR)) {
        perror("Event error");
        goto exit;
    }

    /* read file before staring the loop */
    ret = utsyslog_doread(fd);

    /* monitor file updates */
    while (g_running) {
        timeout.tv_sec = 3;
        timeout.tv_nsec = 0;

        /* Sleep until something happens. */
        ret = kevent(kq, NULL, 0, &tevent, 1, &timeout);
        if (ret == -1) {
            perror("kevent wait");
            goto exit;
        } else if (ret > 0) {
            ret = utsyslog_doread(fd);
        }
    }

exit:
    if (fd > 0) {
        close(fd);
    }

    return (ret);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utsyslog_doread
 *  Description:  Read fd until no data is returned
 * =====================================================================================
 */
static int utsyslog_doread(int fd)
{
    uint16_t i = 0, j = 0;
    int rcnt = 0;
    char *buffer;
    char *line;

    line = (char *)malloc(UT_FRM_SZ);
    buffer = (char *)malloc(UT_FRM_SZ);

    memset(line, 0, UT_FRM_SZ);

    do {
        i = 0;
        memset(buffer, 0, UT_FRM_SZ);

        rcnt = read(fd, buffer, UT_FRM_SZ);

        while (i < rcnt) {
            while ((buffer[i] != '\n') && (i < rcnt) && (j < UT_FRM_SZ)) {
                line[j++] = buffer[i++];
            }

            if ((buffer[i] == '\n') && (j < UT_FRM_SZ)) {
                utrace_print(line);
                memset(line, 0, UT_FRM_SZ);
                j = 0;
                i++; /* reset line and skip the terminate char */
            } else if (j == UT_FRM_SZ) {
                utrace_print(line);
                memset(line, 0, UT_FRM_SZ);
                j = 0;
                i = rcnt; /* reset line and skip the remaining buffer */
            }
        }
    } while (rcnt > 0);

    free(buffer);
    free(line);

    return (rcnt);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Main function entry for UTSYSLOG
 * =====================================================================================
 */
int main(int argc, char *argv[])
{
    int c;
    bool help = false;
    bool daemon = false;
    char *fname;
    int long_index = 0;

    /*	options	descriptor */
    struct option longopts[] = {{"input", required_argument, NULL, 'i'},
                                {"daemon", no_argument, NULL, 'd'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    while ((c = getopt_long(argc, argv, "d::i:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'd':
            daemon = true;
            break;

        case 'i':
            fname = optarg;
            break;

        case 'h':
        default:
            help = true;
            break;
        }
    }

    if (help) {
        printf("UTSYSLOG %s: Syslog to utrace message monitor\n\n", UTSYSLOGVER);
        printf("Usage: utsyslog --input <path>\n\n");
        printf("  Options:\n");
        printf("    --daemon, -d      <path>    Application will deamonize\n");
        printf("    --input, -i       <path>    Path to syslog messages file\n");
        printf("  Help:\n");
        printf("    --help, -h                 Print this help\n\n");

        exit(EXIT_SUCCESS);
    }

    /* daemonize if requested */
    if (daemon) {
        utsyslog_daemonize();
        utsyslog_create_pidfile();
    }

    signal(SIGINT, utsyslog_terminate);
    signal(SIGTERM, utsyslog_terminate);

    utrace_init(UTRACEAPPID, (uint32_t)getpid());
    utrace_settime_cb(&systime);

    if (utrace_open() >= 0) {
        g_running = true;
        utrace_add_info(UTSYSLOG_START, NULL, 0, true);
        utsyslog_run(fname);
    } else {
        perror("main: cannot open utrace");
        exit(EXIT_FAILURE);
    }

    utsyslog_terminate(0);

    exit(EXIT_SUCCESS);
}
