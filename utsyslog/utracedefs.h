/* Application identification data */
#define UTRACEAPPID 0xFFFC
#define UTRACEAPPNAME "utsyslog"

/* Application UTrace hashes go here */
enum { UTSYSLOG_SELECT_FAIL = 1, UTSYSLOG_START, UTSYSLOG_TERMINATE, UTRACEENTRYEND };

/* UTrace hashes description goes into this table */
#ifdef UTRACEGEN
utentry_t utraceentries[UTRACEENTRYEND] =
    {{UTSYSLOG_SELECT_FAIL,
      UTRACEAPPID,
      "Select for syslog file failed",
      "Select for syslog file failed",
      UTRACEAPPNAME},
     {UTSYSLOG_START, UTRACEAPPID, "Started", "UTSYSLOG start", UTRACEAPPNAME},
     {UTSYSLOG_TERMINATE, UTRACEAPPID, "Terminate", "UTSYSLOG terminate", UTRACEAPPNAME},
     {UTRACEENTRYEND,
      UTRACEAPPID,
      "Dummy end message",
      "This message will not be used for decoding",
      UTRACEAPPNAME}};
#endif /* UTRACEGEN */
