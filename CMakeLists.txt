project(utracesys)
cmake_minimum_required(VERSION 2.8)

option(WITH_TEST "with(out) test components")
option(WITH_SYSLOG "with(out) utsyslog component")
option(WITH_RCD "with(out) rc.d service instalation")

add_definitions(-DUT_SERIALIZE_DATA)

add_subdirectory(manuals)
add_subdirectory(libutrace)
add_subdirectory(utrace)
add_subdirectory(utraced)
add_subdirectory(utracedec)
add_subdirectory(utracegen)
add_subdirectory(utracemon)
add_subdirectory(utraceser)

if(WITH_SYSLOG)
    add_subdirectory(utsyslog)
endif(WITH_SYSLOG)

if(WITH_TEST)
    add_subdirectory(testing)
endif(WITH_TEST)

if(WITH_RCD)
    add_subdirectory(service)
endif(WITH_RCD)
