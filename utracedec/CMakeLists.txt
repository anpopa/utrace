include_directories(${utracesys_SOURCE_DIR}/utracedec)
include_directories(${utracesys_SOURCE_DIR}/libutrace)

add_executable(utracedec utracedec.c transfer.c)
target_link_libraries(utracedec utracesys pthread)

install(TARGETS utracedec DESTINATION bin)
