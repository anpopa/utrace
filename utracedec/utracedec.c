/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "utracedec.h"
#include <ut_types.h>

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static void utracedec_print_header(void);

static void utracedec_print_record(utrecord_t *record);

static void utracedec_print_file_marker(utframe_t *frame, uint32_t count);

static void utracedec_print_file_payload(utframe_t *frame, uint32_t count);

static void utracedec_print_textline(utframe_t *frame, char *appname, uint32_t count);

static int utracedec_read_frame(utframe_t *frame, FILE *file);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_init
 *  Description:  Init utracedec data
 * =====================================================================================
 */
static int utracedec_init(utracedec_t *dec)
{
    if (transfer_init(&dec->ft)) {
        return (-1);
    }

    dec->dfn = (char *)malloc(UT_FILENAME_LEN + 1);
    dec->tfn = (char *)malloc(UT_FILENAME_LEN + 1);
    dec->ddn = (char *)malloc(UT_FILENAME_LEN + 1);

    memset(dec->dfn, 0, UT_FILENAME_LEN + 1);
    memset(dec->ddn, 0, UT_FILENAME_LEN + 1);
    memset(dec->tfn, 0, UT_FILENAME_LEN + 1);

    return (ut_decoder_init(&dec->decoder));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_deinit
 *  Description:  Clean utracedec initialization data
 * =====================================================================================
 */
static int utracedec_deinit(utracedec_t *dec)
{
    if (dec->dfn) {
        free(dec->dfn);
    }

    if (dec->tfn) {
        free(dec->tfn);
    }

    if (dec->ddn) {
        free(dec->ddn);
    }

    ut_decoder_free(&dec->decoder);
    transfer_uninit(&dec->ft);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_build_decoder
 *  Description:  Parse decoder file
 * =====================================================================================
 */
static int utracedec_build_decoder(utracedec_t *dec)
{
    FILE *dfile;
    int status = 0;
    char *lpath = NULL;
    char *nline;

    dfile = fopen(dec->dfn, "r");
    if (!dfile) {
        return (-1);
    }

    lpath = (char *)malloc(UT_FILENAME_LEN + 1);
    if (!lpath) {
        return (-1);
    }

    while (!feof(dfile) && (status >= 0)) {
        memset(lpath, 0, sizeof(UT_FILENAME_LEN) + 1);

        if (!fgets(lpath, UT_FILENAME_LEN, dfile)) {
            break;
        }

        if ((nline = strchr(lpath, '\n')) != NULL) {
            *nline = '\0';
        }

        status = ut_decoder_add_file(&dec->decoder, lpath);

        if (status < 0) {
            printf("utracedec: cannot add decoder file at path: %s\n", lpath);
        }
    }

    free(lpath);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_decode_target
 *  Description:  Decode target file
 * =====================================================================================
 */
static int utracedec_decode_target(utracedec_t *dec)
{
    FILE *tfile;
    utframe_t *frame;
    utrecord_t *record;
    utpacketinfo_t *pkt;
    char appname[UT_APPNAME_LEN];
    uint8_t i;
    uint32_t count = 0;
    int status;

    tfile = fopen(dec->tfn, "r");

    if (!tfile) {
        return (-1);
    }

    frame = (utframe_t *)malloc(sizeof(utframe_t));
    record = (utrecord_t *)malloc(sizeof(utrecord_t));
    pkt = (utpacketinfo_t *)malloc(sizeof(utpacketinfo_t));

    if (!frame || !record || !pkt) {
        return (-1);
    }

    utracedec_print_header();

    while (!feof(tfile)) {
        memset(frame, 0, sizeof(utframe_t));

        status = utracedec_read_frame(frame, tfile);

        if ((status >= 0) && (frame->sof == UTSOF)) {
            switch (frame->typ) {
            case utfpkt:
                for (i = 0; i < UT_FRMPKT_CNT; i++) {
                    /* If hash is zero exit for loop */
                    if (frame->pld.pks[i].hsh == 0) {
                        break;
                    }

                    memset(pkt, 0, sizeof(utpacketinfo_t));
                    pkt->aid = frame->aid;
                    pkt->pid = frame->pid;
                    pkt->hsh = frame->pld.pks[i].hsh;
                    pkt->tms = frame->tms | frame->pld.pks[i].tms;
                    memcpy(pkt->dat, &frame->pld.pks[i].dat[0], UT_PKTPLD_SZ);

                    (void)ut_decoder_apply(&dec->decoder, pkt, record);

                    record->cnt = count++;
                    utracedec_print_record(record);
                }
                break;

            case utffst:
                /* Handle file transfer markers */
                utracedec_print_file_marker(frame, count++);
                transfer_start(&dec->ft, frame);
                break;

            case utffso:
                /* Handle file transfer markers */
                utracedec_print_file_marker(frame, count++);
                transfer_stop(&dec->ft, frame);
                break;

            case utffpl:
                /* Handle file transfer payload */
                utracedec_print_file_payload(frame, count++);
                transfer_push_data(&dec->ft, frame);
                break;

            case utfbin:
            case utftxt:
                memset(appname, 0, UT_APPNAME_LEN);
                ut_decoder_get_appname(&dec->decoder, frame->aid, appname);
                utracedec_print_textline(frame, appname, count++);
                break;

            default:
                break;
            }
        } else {
            /* frame corrupted */
        }
    }

    free(frame);
    free(record);
    free(pkt);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_read_frame
 *  Description:  Read one frame from file and decode
 * =====================================================================================
 */
static int utracedec_read_frame(utframe_t *frame, FILE *file)
{
    int status = 0;

    status = (int)fread(frame, sizeof(utframe_t), 1, file);

    if (status < 0) {
        status = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_print_header
 *  Description:  Print header line to stdout
 * =====================================================================================
 */
static void utracedec_print_header(void)
{
    printf("UTRACEDEC version: %s\n", UTRACEVERSION);
    printf("[%8s][%-20s ][%4s:%-7s][%-24s] %s\n",
           "COUNT",
           "TIMESTAMP",
           "APPID",
           "PID",
           "PAYLOAD",
           "APP NAME -> HEADLINE");
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_print_file_marker
 *  Description:  Print file transfer markers
 * =====================================================================================
 */
static void utracedec_print_file_marker(utframe_t *frame, uint32_t count)
{
    char tbuf[30];

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&frame->tms));

    if (frame->typ == utffst) {
        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %s\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "FILE_TRANSFER_START",
               "FILE",
               (char *)&frame->pld.txt[0]);
    } else if (frame->typ == utffso) {
        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %s\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "FILE_TRANSFER_END",
               "FILE",
               (char *)&frame->pld.txt[0]);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_print_file_payload
 *  Description:  Print file transfer payload frames
 * =====================================================================================
 */
static void utracedec_print_file_payload(utframe_t *frame, uint32_t count)
{
    uint32_t int1, int2, int3, int4;
    char tbuf[30];

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&frame->tms));

    if (frame->typ == utffpl) {
        int1 = (uint32_t)(frame->pld.dat[3] << 24 | frame->pld.dat[2] << 16 | frame->pld.dat[1] << 8
                          | frame->pld.dat[0]);
        int2 = (uint32_t)(frame->pld.dat[7] << 24 | frame->pld.dat[6] << 16 | frame->pld.dat[5] << 8
                          | frame->pld.dat[4]);
        int3 = (uint32_t)(frame->pld.dat[11] << 24 | frame->pld.dat[10] << 16
                          | frame->pld.dat[9] << 8 | frame->pld.dat[8]);
        int4 = (uint32_t)(frame->pld.dat[15] << 24 | frame->pld.dat[14] << 16
                          | frame->pld.dat[13] << 8 | frame->pld.dat[12]);

        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %08x%08x%08x%08x...\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "FILE_TRANSFER_DATA",
               "FILE",
               int4,
               int3,
               int2,
               int1);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_print_textline
 *  Description:  Print one record line
 * =====================================================================================
 */
static void utracedec_print_textline(utframe_t *frame, char *appname, uint32_t count)
{
    char tbuf[30];
    uint32_t int1, int2, int3, int4;

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&frame->tms));

    if (frame->typ == utftxt) {
        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %s\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "TEXT",
               appname,
               (char *)&frame->pld.txt[0]);
    } else {
        int1 = (uint32_t)(frame->pld.dat[3] << 24 | frame->pld.dat[2] << 16 | frame->pld.dat[1] << 8
                          | frame->pld.dat[0]);
        int2 = (uint32_t)(frame->pld.dat[7] << 24 | frame->pld.dat[6] << 16 | frame->pld.dat[5] << 8
                          | frame->pld.dat[4]);
        int3 = (uint32_t)(frame->pld.dat[11] << 24 | frame->pld.dat[10] << 16
                          | frame->pld.dat[9] << 8 | frame->pld.dat[8]);
        int4 = (uint32_t)(frame->pld.dat[15] << 24 | frame->pld.dat[14] << 16
                          | frame->pld.dat[13] << 8 | frame->pld.dat[12]);

        printf("[%08x][%20s ][%04x:%08x][%-24s] %s -> %08x%08x%08x%08x...\n",
               count,
               tbuf,
               frame->aid,
               frame->pid,
               "BINARY",
               appname,
               int4,
               int3,
               int2,
               int1);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utracedec_print_record
 *  Description:  Print one record line
 * =====================================================================================
 */
static void utracedec_print_record(utrecord_t *record)
{
    char tbuf[30];
    uint32_t int1, int2, int3;

    int1 = (uint32_t)(record->pkt.dat[3] << 24 | record->pkt.dat[2] << 16 | record->pkt.dat[1] << 8
                      | record->pkt.dat[0]);
    int2 = (uint32_t)(record->pkt.dat[7] << 24 | record->pkt.dat[6] << 16 | record->pkt.dat[5] << 8
                      | record->pkt.dat[4]);
    int3 = (uint32_t)(record->pkt.dat[11] << 24 | record->pkt.dat[10] << 16
                      | record->pkt.dat[9] << 8 | record->pkt.dat[8]);

    strftime(tbuf, 30, "%H:%M:%S %Y-%m-%d", localtime((time_t *)&record->pkt.tms));

    printf("[%08x][%20s ][%04x:%08x][%08x%08x%08x] %s -> %s\n",
           record->cnt,
           tbuf,
           record->pkt.aid,
           record->pkt.pid,
           int3,
           int2,
           int1,
           record->name,
           record->hline);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Main function for UTraceDec tool
 * =====================================================================================
 */
int main(int argc, char *argv[])
{
    int c;
    bool help = false;
    utracedec_t *decoder;
    int long_index = 0;

    /*	options	descriptor */
    struct option longopts[] = {{"list", required_argument, NULL, 'l'},
                                {"file", required_argument, NULL, 'f'},
                                {"dump", required_argument, NULL, 'd'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    decoder = (utracedec_t *)malloc(sizeof(utracedec_t));

    if (utracedec_init(decoder) < 0) {
        exit(EXIT_FAILURE);
    }

    while ((c = getopt_long(argc, argv, "l:f:d:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'l':
            strncpy(decoder->dfn, optarg, UT_FILENAME_LEN);
            break;

        case 'd':
            strncpy(decoder->ddn, optarg, UT_FILENAME_LEN);
            break;

        case 'f':
            strncpy(decoder->tfn, optarg, UT_FILENAME_LEN);
            break;

        case 'h':
        default:
            help = true;
            break;
        }
    }

    if ((strlen(decoder->dfn) == 0) && (strlen(decoder->tfn) == 0)) {
        utracedec_deinit(decoder);
        help = true;
    }

    if (strlen(decoder->ddn) >= 1) {
        if (transfer_set_path(&decoder->ft, decoder->ddn) < 0) {
            printf("utracedec: cannot create files in: %s\n", decoder->ddn);
            utracedec_deinit(decoder);
            help = true;
        }
    }

    if (help) {
        printf("UTraceDec %s: Decode one trace dump using decoder file\n\n", UTRACEVERSION);
        printf("Usage: utracedec --list <decoders> --file <tracefile>\n\n");
        printf("  Options:\n");
        printf("    --list, -l       <path>    UTrace decoders list file path\n");
        printf("    --file, -f       <path>    Target dump file to be postprocesed\n");
        printf("    --dump, -d       <path>    Path for file transfer location\n\n");
        printf("  Help:\n");
        printf("    --help, -h                 Print this help\n\n");

        exit(EXIT_SUCCESS);
    }

    if (utracedec_build_decoder(decoder) < 0) {
        printf("utracedec: cannot parse decoder file %s\n", decoder->dfn);
    } else if (utracedec_decode_target(decoder) < 0) {
        printf("utracedec: cannot parse target file %s\n", decoder->tfn);
    } else {
        /* noop */
    }

    utracedec_deinit(decoder);
    free(decoder);

    exit(EXIT_SUCCESS);
}
