/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "transfer.h"
#include <libgen.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define W_BUF_SZ 512

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static int transfer_parse_start_frame(transfer_t *ft, utframe_t *frame);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_parse_start_frame
 *  Description:  Parse the start frame payload for file name and size
 * =====================================================================================
 */
static int transfer_parse_start_frame(transfer_t *ft, utframe_t *frame)
{
    int status = 0;
    char *token, *string, *sref;

    string = strdup((char *)&frame->pld.txt);

    if (string != NULL) {
        int idx = 0;
        sref = string;

        while ((token = strsep(&string, ";")) != NULL) {
            if (idx == 0) {
                memcpy(ft->file->name, token, strlen(token));
            } else if (idx == 1) {
                ft->file->fsize = atol(token);
            } else {
                /* noop */
            }

            idx++;
        }

        free(sref);
    } else {
        status = -1;
    }

    ft->file->id = frame->pid;

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_init
 *  Description:  Initialize transfer module
 * =====================================================================================
 */
int transfer_init(transfer_t *ft)
{
    int status = 0;

    ft->run = false;

    ft->file = (file_t *)malloc(sizeof(file_t));
    memset(ft->file, 0, sizeof(file_t));

    sprintf(ft->file->path, ".");

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_set_path
 *  Description:  Set file transfer path
 * =====================================================================================
 */
int transfer_set_path(transfer_t *ft, const char *path)
{
    int status = 0;

    if (!ft->run) {
        struct stat path_stat;

        sprintf(ft->file->path, "%s", path);
        if (stat(ft->file->path, &path_stat) >= 0) {
            if (!S_ISDIR(path_stat.st_mode)) {
                status = -1;
            }
        } else {
            status = -1;
        }
    } else {
        status = -1;
    }
    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_start
 *  Description:  Start thread in transfer module
 * =====================================================================================
 */
int transfer_start(transfer_t *ft, utframe_t *frame)
{
    int status = 0;

    if (ft->run) {
        printf("utracedec: new transfer started while another in progress\n");
        return (-1);
    }

    if (transfer_parse_start_frame(ft, frame) < 0) {
        return (-1);
    }

    /* create the temp file */
    sprintf(ft->file->ptmp, "%s/.%08x.utracetrans", ft->file->path, ft->file->id);
    ft->file->ftmp = fopen(ft->file->ptmp, "w");
    if (!ft->file->ftmp) {
        return (-1);
    }

    /* ready to process data frames */
    ft->run = true;

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_stop
 *  Description:  Stop thread in transfer module
 * =====================================================================================
 */
int transfer_stop(transfer_t *ft, utframe_t *frame)
{
    int status = 0;
    uint8_t *buf = NULL;

    if (!ft->run) {
        status = -1;
        goto exit;
    }

    if (frame->pid != ft->file->id) {
        status = -2;
        goto exit;
    }

    if (ft->file->tframes < (ft->file->fsize / sizeof(utframe_t))) {
        status = -3;
        printf("utracedec: missing frames while transfer file: %s. Abandon", ft->file->name);
        goto exit;
    }

    buf = (uint8_t *)malloc(W_BUF_SZ);

    /* close temp the file and reopen for read */
    fclose(ft->file->ftmp);
    ft->file->ftmp = fopen(ft->file->ptmp, "r");

    /* create the target file */
    sprintf(ft->file->pnew, "%s/%08x-%s", ft->file->path, ft->file->id, ft->file->name);
    ft->file->fnew = fopen(ft->file->pnew, "w");

    if (ft->file->fnew != NULL) {
        int rcnt = 0, cnt = 0;

        fseek(ft->file->ftmp, 0L, SEEK_SET);

        while (cnt < ft->file->fsize) {
            memset(buf, 0, W_BUF_SZ);
            rcnt = fread(buf, 1, W_BUF_SZ, ft->file->ftmp);
            if (rcnt > 0) {
                /* Temp file will have at most an empty extra frame */
                if ((rcnt + cnt) <= ft->file->fsize) {
                    fwrite(buf, 1, rcnt, ft->file->fnew);
                } else {
                    fwrite(buf, 1, (ft->file->fsize - cnt), ft->file->fnew);
                }
                cnt = cnt + rcnt;
            } else {
                if (cnt != ft->file->fsize) {
                    printf("utracedec: the file is corrupted: %s", ft->file->pnew);
                    cnt = ft->file->fsize; /* make sure we terminate the read loop */
                    status = -4;
                }
            }
        }
    } else {
        status = -5;
    }

exit:
    ft->run = false;

    if (buf) {
        free(buf);
    }

    if (ft->file->ftmp) {
        fclose(ft->file->ftmp);
        ft->file->ftmp = NULL;
    }

    if (ft->file->fnew) {
        fclose(ft->file->fnew);
        ft->file->fnew = NULL;
    }

    /* remove temp file */
    (void)remove(ft->file->ptmp);

    /* clear file information for next transfer */
    memset(ft->file->name, 0, UT_FILENAME_LEN);
    memset(ft->file->ptmp, 0, UT_FILENAME_LEN);
    memset(ft->file->pnew, 0, UT_FILENAME_LEN);
    ft->file->id = 0;
    ft->file->fsize = 0;
    ft->file->tsize = 0;
    ft->file->tframes = 0;

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_uninit
 *  Description:  Uninitialize transfer module
 * =====================================================================================
 */
int transfer_uninit(transfer_t *ft)
{
    int status = 0;

    ft->run = false;

    if (ft->file->ftmp) {
        fclose(ft->file->ftmp);
        ft->file->ftmp = NULL;
    }

    if (ft->file->fnew) {
        fclose(ft->file->fnew);
        ft->file->fnew = NULL;
    }

    if (strlen(ft->file->ptmp) > 0) {
        /* remove temp file */
        (void)remove(ft->file->ptmp);
    }

    free(ft->file);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_push
 *  Description:  Push a frame for transfer module
 * =====================================================================================
 */
int transfer_push_data(transfer_t *ft, utframe_t *frame)
{
    int status = 0;

    if ((ft->run == true) && (frame->pid == ft->file->id)) {
        ft->file->tsize =
            ft->file->tsize + fwrite((char *)frame->pld.dat, 1, UT_FRMDAT_SZ, ft->file->ftmp);
        ft->file->tframes++;
    } else {
        status = -1;
    }

    return (status);
}
