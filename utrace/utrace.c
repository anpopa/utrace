/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "utrace.h"
#include <getopt.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

enum { ACTBIN = 0, ACTSTR, ACTSTD, ACTFLS };

static inline void systime(uint64_t *ct)
{
    *ct = (uint64_t)time(NULL);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  print_stdin
 *  Description:  Read stdin and print messages line by line
 * =====================================================================================
 */
static void print_stdin(void)
{
    char *buffer;
    bool done = false;

    buffer = (char *)malloc(UT_FRMDAT_SZ);

    while (!feof(stdin) && !done) {
        memset(buffer, 0, UT_FRMDAT_SZ);

        if (fgets(buffer, UT_FRMDAT_SZ, stdin) != NULL) {
            if (buffer[strlen(buffer) - 1] == '\n') {
                buffer[strlen(buffer) - 1] = 0;
            }

            utrace_print(buffer);
        } else {
            done = true;
        }
    }

    free(buffer);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  UTrace main function.
 * =====================================================================================
 */
int main(int argc, char *argv[])
{
    const char *string = NULL;
    int long_index = 0;
    bool help = false;
    uint16_t hash = 0;
    uint64_t data = 0;
    int act = -1;
    int c;

    /*	options	descriptor */
    struct option longopts[] = {{"hash", required_argument, NULL, 'a'},
                                {"data", required_argument, NULL, 'd'},
                                {"string", required_argument, NULL, 's'},
                                {"stdin", no_argument, NULL, 'i'},
                                {"file", required_argument, NULL, 'f'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    while ((c = getopt_long(argc, argv, "a:d:s:f:i:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'a':
            hash = (uint16_t)atoi(optarg);
            act = ACTBIN;
            break;

        case 'd':
            data = (uint64_t)atol(optarg);
            break;

        case 's':
            string = optarg;
            act = ACTSTR;
            break;

        case 'i':
            act = ACTSTD;
            break;

        case 'f':
            string = optarg;
            act = ACTFLS;
            break;

        case 'h':
        default:
            help = true;
            break;
        }
    }

    switch (act) {
    case ACTBIN:
        if (hash == 0) {
            help = true;
        }
        break;

    case ACTSTR:
        if (string == NULL) {
            help = true;
        }
        break;

    case ACTSTD:
        break;

    case ACTFLS:
        if (string == NULL) {
            help = true;
        }
        break;

    default:
        help = true;
    }

    if (help) {
        printf("UTrace %s: Send one UTrace entry to UTrace pipe\n\n", UTRACEVERSION);
        printf("Usage: utrace --string \"<string>\"\n");
        printf("Usage: utrace --hash <word> --data <long>\n");
        printf("Usage: utrace --file \"<fullpath>\"\n");
        printf("Usage: utrace --stdin \n\n");
        printf("  Binary:\n");
        printf("    --hash, -a    <word>      Two bytes hash ID\n");
        printf("    --data, -d    <long>      Payload data\n\n");
        printf("  String:\n");
        printf("    --string, -s  <string>    Send string to utrace\n\n");
        printf("  Standard input:\n");
        printf("    --stdin, -i               Read from stdin until EOF\n\n");
        printf("  Filedump:\n");
        printf("    --file, -f    <string>    Full path to file to be dumped\n");
        printf("                              Note: utraced process need to have read access to "
               "this file\n\n");
        printf("  Help:\n");
        printf("    --help, -h                Print this help\n\n");

        exit(EXIT_SUCCESS);
    }

    utrace_init(UTRACEAPPID, (uint32_t)getpid());
    utrace_settime_cb(&systime);

    if (utrace_open() >= 0) {
        switch (act) {
        case ACTBIN:
            utrace_add_error(hash, &data, sizeof(data), true);
            break;

        case ACTSTR:
            utrace_print(string);
            break;

        case ACTSTD:
            print_stdin();
            break;

        case ACTFLS:
            utrace_dump_file(string);
            break;

        default:
            printf("UTrace: Requested action error\n");
        }
    } else {
        printf("UTrace: Connection error\n");
    }

    utrace_free();

    exit(EXIT_SUCCESS);
}
