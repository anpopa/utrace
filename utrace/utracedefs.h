/* Application identification data */
#define UTRACEAPPID 0xFFFD
#define UTRACEAPPNAME "utrace"

/* Application UTrace hashes go here */
enum { UTRACEPUSHDEFAUL = 1, UTRACEENTRYEND };

/* UTrace hashes description goes into this table */
#ifdef UTRACEGEN
utentry_t utraceentries[UTRACEENTRYEND] = {{UTRACEPUSHDEFAUL,
                                            UTRACEAPPID,
                                            "Pushed message from system",
                                            "This message will not be used for decoding",
                                            UTRACEAPPNAME},
                                           {UTRACEENTRYEND,
                                            UTRACEAPPID,
                                            "Dummy end message",
                                            "This message will not be used for decoding",
                                            UTRACEAPPNAME}};
#endif /* UTRACEGEN */
