/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ut_compress.h>

#define UTESTMAXSTR 255

static bool utrace_unittest_compress(void)
{
    const char *teststr = "acddddddef2ffff123456678991zzzzzzzzaa88a88a99asdfgnmmm";
    char *infstr;
    char *defstr;
    int outsize;
    bool result = false;

    infstr = (char *)malloc(UTESTMAXSTR);
    defstr = (char *)malloc(UTESTMAXSTR);

    printf("ut_compress_string_inflate:\n");
    printf("In[%ld]: %s\n", strlen(teststr), teststr);
    memset(infstr, 0, UTESTMAXSTR);
    outsize = ut_compress_string_inflate((uint8_t *)infstr,
                                         (uint8_t *)teststr,
                                         (uint32_t)strlen(teststr));
    printf("Out[%d]: %s\n", outsize, infstr);
    printf("\n");

    printf("ut_compress_string_deflate:\n");
    printf("In[%ld]: %s\n", strlen(infstr), infstr);
    memset(defstr, 0, UTESTMAXSTR);
    outsize =
        ut_compress_string_deflate((uint8_t *)defstr, (uint8_t *)infstr, (uint32_t)strlen(infstr));
    printf("Out[%d]: %s\n", outsize, defstr);
    printf("\n");

    if (!strcmp(teststr, defstr)) {
        result = true;
    }

    free(infstr);
    free(defstr);

    return (result);
}

int main(int argc, char *argv[])
{
    printf("Unit test for ut_compress\n");
    printf("-------------------------\n");

    if (utrace_unittest_compress()) {
        printf("Result: PASSED\n");
    } else {
        printf("Result: FAILED\n");
    }

    printf("-------------------------\n");

    exit(EXIT_SUCCESS);
}
