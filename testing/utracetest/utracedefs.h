/* Application identification data */
#define UTRACEAPPID 0x01
#define UTRACEAPPNAME "utracetest"

/* Hash IDs go here */
enum { THREAD_START = 1, THREAD_END, THREAD_LOOP_TID, THREAD_LOOP_DAT, UTRACEENTRYEND };

/* Hash enties description goes into this table */
#ifdef UTRACEGEN
utentry_t utraceentries[UTRACEENTRYEND] =
    {{THREAD_START,
      UTRACEAPPID,
      "Test thread loop starts",
      "It markes the start of for loop for this test thread",
      UTRACEAPPNAME},
     {THREAD_END,
      UTRACEAPPID,
      "Test thread loop ends",
      "It markes the end of for loop for this test thread",
      UTRACEAPPNAME},
     {THREAD_LOOP_TID, UTRACEAPPID, "New loop for thread ID", " ", UTRACEAPPNAME},
     {THREAD_LOOP_DAT,
      UTRACEAPPID,
      "User data for current loop",
      "It is incremented with each loop",
      UTRACEAPPNAME},
     {UTRACEENTRYEND,
      UTRACEAPPID,
      "End message",
      "This message will not be used for decoding",
      UTRACEAPPNAME}};
#endif /* UTRACEGEN */
