include_directories(${utracesys_SOURCE_DIR}/testing/utracetest)
include_directories(${utracesys_SOURCE_DIR}/libutrace)
add_executable(utracetest utracetest.c)
target_link_libraries(utracetest utracesys pthread)
