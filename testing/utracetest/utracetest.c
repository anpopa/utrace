/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "ut_client_app.h"
#include "utracedefs.h"
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

int g_cntloop;
int g_cntusec;

void *tracemsg(void *threadid)
{
    long tid;
    uint32_t i;

    tid = (long)threadid;
    uint32_t dat = 0;
    char *msg;

    msg = malloc(120);

    utrace_add_info(THREAD_START, &tid, sizeof(tid), true);
    for (i = 0; i < g_cntloop; i++) {
        dat = (uint16_t)tid << 24 | i;

        snprintf(msg, 120, "New loop started for thread %ld count %d", tid, i);
        utrace_print(msg);

        utrace_add_info(THREAD_LOOP_TID, &tid, sizeof(tid), true);
        usleep(g_cntusec);

        utrace_add_info(THREAD_LOOP_DAT, &dat, sizeof(dat), true);
        usleep(g_cntusec);
    }
    utrace_add_info(THREAD_END, &tid, sizeof(tid), true);

    free(msg);

    pthread_exit(NULL);
}

void mytime(uint64_t *ct)
{
    *ct = (uint64_t)time(NULL);
}

int main(int argc, char *argv[])
{
    long t;
    int rc;
    int c;
    bool help = false;
    int cntthr = 0;
    int cntloop = 0;
    int cntusec = 0;

    pthread_t threads[10000];

    signal(SIGPIPE, SIG_IGN);

    while ((c = getopt(argc, argv, "t:l:u:h")) != -1) {
        switch (c) {
        case 't':
            cntthr = atoi(optarg) < 1000 ? atoi(optarg) : 1000;
            break;

        case 'l':
            cntloop = atoi(optarg);
            break;

        case 'u':
            cntusec = atoi(optarg);
            break;
            break;

        case 'h':
        default:
            help = true;
            break;
        }
    }

    if ((cntthr <= 0) || (cntloop <= 0) || (cntusec <= 0)) {
        help = true;
    }

    if (help) {
        printf("UTraceTest %s: Generate UTrace messages\n\n", UTRACEVERSION);
        printf("Usage: utracetest -t <int> -l <int> -u <int> \n\n");
        printf("  Options:\n");
        printf("\t -t \t <uint_t> \t Number of threads to start (max 10000)\n");
        printf("\t -l \t <uint_t> \t Number of loops per thread\n");
        printf("\t -u \t <uint_t> \t Time delay in usec between messages\n");
        printf("\t -h \t          \t Print this help\n\n");

        exit(EXIT_SUCCESS);
    }

    g_cntloop = cntloop;
    g_cntusec = cntusec;

    utrace_init(UTRACEAPPID, (uint32_t)getpid());
    utrace_settime_cb(&mytime);
    utrace_open();

    utrace_print("Program start threads");
    for (t = 0; t < cntthr; t++) {
        rc = pthread_create(&threads[t], NULL, tracemsg, (void *)t);
        if (rc) {
            printf("ERROR: return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    for (t = 0; t < cntthr; t++) {
        pthread_join(threads[t], NULL);
    }

    t = (long)0xFFDDEEAA;

    utrace_dump(&t, sizeof(long));
    utrace_flush();

    utrace_print("Program end");

    utrace_close();
    utrace_free();

    exit(EXIT_SUCCESS);
}
