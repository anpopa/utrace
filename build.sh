#!/bin/sh

mkdir build 2> /dev/null
cd build
rm -rf *

if [ "$1" ==  "release" ]; then
    echo "Building release version"
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make clean
    make
else
    echo "Building debug version"
    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make clean
    make
fi

cd ..

FBSDMANIFEST="./packages/freebsd/MANIFEST"
FBSDBUILDSH="./packages/freebsd/buildpkg.sh"
FBSDROOT="./build/freebsd/root"
FBSDOUTDIR="./build/freebsd/outdir"

if [ "FreeBSD" = $(uname) ]; then

    # create the directory structure
    mkdir -p $FBSDROOT/usr/local/bin
    mkdir -p $FBSDROOT/usr/local/include/utrace
    mkdir -p $FBSDROOT/usr/local/lib/utrace
    mkdir -p $FBSDROOT/usr/local/etc/rc.d
    mkdir -p $FBSDROOT/usr/local/man/man1
    mkdir -p $FBSDROOT/usr/local/man/man8
    
    # copy executable files
    cp ./build/utrace/utrace $FBSDROOT/usr/local/bin/
    cp ./build/utraced/utraced $FBSDROOT/usr/local/bin/
    cp ./build/utracedec/utracedec $FBSDROOT/usr/local/bin/
    cp ./build/utracemon/utracemon $FBSDROOT/usr/local/bin/
    cp ./build/utraceser/utraceser $FBSDROOT/usr/local/bin/
    cp ./build/utracegen/utracegen $FBSDROOT/usr/local/bin/
    cp ./build/utsyslog/utsyslog $FBSDROOT/usr/local/bin/
    
    # copy include files
    cp ./libutrace/ut_config.h $FBSDROOT/usr/local/include/utrace
    cp ./libutrace/ut_types.h $FBSDROOT/usr/local/include/utrace
    cp ./libutrace/ut_client.h $FBSDROOT/usr/local/include/utrace
    cp ./libutrace/ut_client_app.h $FBSDROOT/usr/local/include/utrace
    cp ./libutrace/ut_single_app.h $FBSDROOT/usr/local/include/utrace
    
    # copy the library files
    cp ./build/libutrace/*.a $FBSDROOT/usr/local/lib/utrace

    # copy the service file for freebsd
    cp ./service/utraced $FBSDROOT/usr/local/etc/rc.d
    cp ./service/utsyslogd $FBSDROOT/usr/local/etc/rc.d
    
    # copy the manual pages
    cp ./manuals/utrace.1.gz $FBSDROOT/usr/local/man/man1/
    cp ./manuals/utraced.8.gz $FBSDROOT/usr/local/man/man8/
    cp ./manuals/utsyslog.8.gz $FBSDROOT/usr/local/man/man8/
    cp ./manuals/utracedec.1.gz $FBSDROOT/usr/local/man/man1/
    cp ./manuals/utracemon.1.gz $FBSDROOT/usr/local/man/man1/
    cp ./manuals/utraceser.1.gz $FBSDROOT/usr/local/man/man1/
    cp ./manuals/utracegen.1.gz $FBSDROOT/usr/local/man/man1/

    mkdir -p $FBSDOUTDIR
    export PVERSION="r$(svnversion)"
    sh $FBSDBUILDSH $FBSDMANIFEST $FBSDROOT $FBSDOUTDIR
fi

