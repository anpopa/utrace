/* Application identification data */
#define UTRACEAPPID 0xFFFE
#define UTRACEAPPNAME "utraced"

/* Hash IDs go here */
enum {
    UTRACED_MONITOR_CONNECTED = 1,
    UTRACED_MONITOR_DISCONNECTED,
    UTRACED_MAIN_SELECT_FAILED,
    UTRACED_READ_SELECT_FAILED,
    UTRACED_TERMINATE,
    UTRACED_STARTED,
    UTRACED_FT_EACCES,
    UTRACEENTRYEND
};

/* Hash enties description goes into this table */
#ifdef UTRACEGEN
utentry_t utraceentries[UTRACEENTRYEND] =
    {{UTRACED_MONITOR_CONNECTED,
      UTRACEAPPID,
      "External monitor connected",
      "An external monitor is connected. UTraceD will send live message to this address",
      UTRACEAPPNAME},
     {UTRACED_MONITOR_DISCONNECTED,
      UTRACEAPPID,
      "External monitor disconnected",
      "Connection with external monitor is lost",
      UTRACEAPPNAME},
     {UTRACED_MAIN_SELECT_FAILED,
      UTRACEAPPID,
      "Select in main thread failed",
      "Select in main thread failed",
      UTRACEAPPNAME},
     {UTRACED_READ_SELECT_FAILED,
      UTRACEAPPID,
      "Select in read thread failed",
      "Select in read thread failed",
      UTRACEAPPNAME},
     {UTRACED_TERMINATE, UTRACEAPPID, "Terminate", "UTraceD terminate", UTRACEAPPNAME},
     {UTRACED_STARTED, UTRACEAPPID, "Started", "Start read loop", UTRACEAPPNAME},
     {UTRACED_FT_EACCES,
      UTRACEAPPID,
      "File transfer access error",
      "File transfer access error",
      UTRACEAPPNAME},
     {UTRACEENTRYEND,
      UTRACEAPPID,
      "End message",
      "This message will not be used for decoding",
      UTRACEAPPNAME}};
#endif /* UTRACEGEN */
