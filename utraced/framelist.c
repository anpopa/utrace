/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "framelist.h"

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  frame_list_init
 *  Description:  Initialize frame list
 * =====================================================================================
 */
int frame_list_init(framel_t *l)
{
    l->head = NULL;
    l->size = 0;
    pthread_mutex_init(&l->mtx, NULL);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  frame_list_delete
 *  Description:  Delete list
 * =====================================================================================
 */
int frame_list_delete(framel_t *l)
{
    while (l->head) {
        frame_list_rem(l, NULL);
    }
    pthread_mutex_destroy(&l->mtx);

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  frame_list_size
 *  Description:  Get list size
 * =====================================================================================
 */
uint32_t frame_list_size(framel_t *l)
{
    return (l->size);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  frame_list_add
 *  Description:  Add one frame to list
 * =====================================================================================
 */
int frame_list_add(framel_t *l, utframe_t *d)
{
    utracellel_t *nelem = (utracellel_t *)malloc(sizeof(utracellel_t));
    utracellel_t *tmp;
    int status;

    if (nelem) {
        memcpy(&nelem->dat, d, sizeof(utframe_t));

        pthread_mutex_lock(&l->mtx);
        nelem->n = l->head;
        l->head = nelem;
        l->size++;
        pthread_mutex_unlock(&l->mtx);

        status = 0;
    } else {
        status = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  frame_list_rem
 *  Description:  Remove one element from list
 * =====================================================================================
 */
int frame_list_rem(framel_t *l, utframe_t *d)
{
    utracellel_t *tmp;
    int status = 0;

    tmp = l->head;
    if (tmp) {
        pthread_mutex_lock(&l->mtx);
        l->head = l->head->n;
        l->size--;
        pthread_mutex_unlock(&l->mtx);

        if (d != NULL) {
            memcpy(d, &tmp->dat, sizeof(utframe_t));
        }

        free(tmp);
    } else {
        status = -1;
    }

    return (status);
}
