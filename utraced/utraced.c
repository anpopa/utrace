/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "utraced.h"
#include "transfer.h"
#include "ut_client_app.h"
#include "ut_serialize.h"
#include "utracedefs.h"
#include <stdio.h>

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */
static utraced_t *g_utraced;

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static int utraced_write_frame(utraced_t *utraced, utframe_t *frame);

static int utraced_write_frame_cb(utframe_t *frame);

static int utraced_send_frame(utraced_t *utraced, utframe_t *frame);

static bool utraced_check_bool_string(const char *str, bool def);

static int utraced_new_systrace(utraced_t *utraced, const char *path);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_init
 *  Description:  Initialize application data structure. Create FIFO file
 * =====================================================================================
 */
static int utraced_init(utraced_t *utraced)
{
    int status = 0;

    utraced->rfd = open(UT_PIPE_PTH, O_RDWR | O_NONBLOCK);

    if (utraced->rfd < 0) {
        unlink(UT_PIPE_PTH);
        status = mkfifo(UT_PIPE_PTH, 0666);
        if (status < 0) {
            status = -1;
        } else {
            utraced->rfd = open(UT_PIPE_PTH, O_RDWR | O_NONBLOCK);
            if (utraced->rfd < 0) {
                status = -1;
            }
        }
    }

    if (status >= 0) {
        /* if opath is null will try a default location  */
        if (utraced_new_systrace(utraced, utraced->opt.opath) < 0) {
            status = -1;
        }
    }

    if (status >= 0) {
        if (transfer_init(&utraced->ft) >= 0) {
            transfer_set_write_callback(&utraced->ft, utraced_write_frame_cb);
            status = transfer_start(&utraced->ft);
        }
    }

    if (status >= 0) {
        utraced->con = false;
        utraced->running = true;
        utraced->connfd = 0;
    }

    pthread_mutex_init(&utraced->wmtx, NULL);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_new_systrace
 *  Description:  Create a new systrace file. If a directory is provided use it as path.
 *                If the split option is on we create new files using tracecnt value.
 *                Naming systrace file using time stamp is not sufficient because
 *                the split option allow creating files faster de 1 sec. The tracecnt
 *                is incremented with each new file. This helps also with per session
 *                files identification.
 * =====================================================================================
 */
static int utraced_new_systrace(utraced_t *utraced, const char *path)
{
    int status = 0;
    char *tpath;

    /* before creating a new file make sure we close the current one */
    if (utraced->systrace != NULL) {
        fclose(utraced->systrace);
        utraced->systrace = NULL;
    }

    if (path != NULL) {
        if (strlen(path) > 0) {
            struct stat path_stat;

            tpath = (char *)malloc(UT_FILENAME_LEN);
            memset(tpath, 0, UT_FILENAME_LEN);

            if (stat(path, &path_stat) >= 0) {
                if (S_ISDIR(path_stat.st_mode)) {
                    snprintf(tpath,
                             UT_FILENAME_LEN,
                             "%s/systrace[%d].utrace",
                             path,
                             utraced->tracecnt++);
                    utraced->systrace = fopen(tpath, "w");
                } else {
                    /* a file path has been provided and exist,
                     * in this case we appaned */
                    utraced->systrace = fopen(path, "a");
                }
            } else {
                /* a new file path has been provided */
                utraced->systrace = fopen(path, "w");
            }

            free(tpath);
        } else {
            status = -1;
        }
    } else {
        status = -1;
    }

    if (status < 0) {
        tpath = (char *)malloc(UT_FILENAME_LEN);
        memset(tpath, 0, UT_FILENAME_LEN);

        snprintf(tpath, UT_FILENAME_LEN, "%s/systrace[%d].utrace", path, utraced->tracecnt++);

        /* the path cannot be used - try default location */
        utraced->systrace = fopen(tpath, "w");

        if (utraced->systrace == NULL) {
            status = -1;
        } else {
            status = 1;
        }

        free(tpath);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_read_loop
 *  Description:  Main read loop thread function
 * =====================================================================================
 */
static void *utraced_read_loop(void *p_utraced)
{
    utraced_t *utraced = (utraced_t *)p_utraced;
    utframe_t frame;
    fd_set rfd;
    struct timeval tv;
    int status;
    int idleflush = 0;

    if (utraced->rfd < 0) {
        return (NULL);
    }

    FD_ZERO(&rfd);

    if (utraced->running) {
        utrace_add_error(UTRACED_STARTED, NULL, 0, true);
    }

    while (utraced->running) {
        tv.tv_sec = 3;
        tv.tv_usec = 0;
        FD_SET(utraced->rfd, &rfd);

        memset(&frame, 0, sizeof(utframe_t));
        status = select(utraced->rfd + 1, &rfd, NULL, NULL, &tv);

        if (status < 0) {
            utrace_add_error(UTRACED_READ_SELECT_FAILED, NULL, 0, true);
            utraced->running = false;
        } else if (status) {
            idleflush = 0;
            memset(&frame, 0, sizeof(utframe_t));

            status = (int)read(utraced->rfd, &frame, sizeof(utframe_t));

            if (status < 0) {
                continue;
            }

            /* Check if we have a file transfer request  */
            if (frame.typ == utffrq) {
                transfer_push(&utraced->ft, &frame);
            } else {
                /* Write frame to available sinks */
                utraced_write_frame(utraced, &frame);
            }
        } else {
            /* read is in idle - flash the file stream after 3 idle states */
            if (idleflush++ >= 3) {
                fflush(utraced->systrace);
                idleflush = 0;
            }
        }
    }

    return (NULL);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_write_frame
 *  Description:  Write one frame compressed to systrace
 * =====================================================================================
 */
static int utraced_write_frame(utraced_t *utraced, utframe_t *frame)
{
    int status = 0;

    /* Need to serialize the frame writes */
    pthread_mutex_lock(&utraced->wmtx);

    fwrite(frame, sizeof(utframe_t), 1, utraced->systrace);

    /* Send frame to utracemon if connected */
    if (utraced->con) {
        status = utraced_send_frame(utraced, frame);

        if (status < 0) {
            utraced->con = false;
        }
    }

    pthread_mutex_unlock(&utraced->wmtx);

    /* if split is set to true we asume the limit is properly set*/
    if (utraced->opt.split == true) {
        if (utraced->tracesize >= (utraced->opt.limit * 1024 * 1024)) {
            status = utraced_new_systrace(utraced, utraced->opt.opath);
            utraced->tracesize = 0;
        }
        utraced->tracesize = utraced->tracesize + sizeof(utframe_t);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_write_frame_cb
 *  Description:  Write one frame compressed to systrace
 * =====================================================================================
 */
static int utraced_write_frame_cb(utframe_t *frame)
{
    return (utraced_write_frame(g_utraced, frame));
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_send_frame
 *  Description:  Send one frame to monitor
 * =====================================================================================
 */
static int utraced_send_frame(utraced_t *utraced, utframe_t *frame)
{
    int status = 0;
    uint32_t wsz = 0;
    fd_set wfd;
    struct timeval tv;
    uint8_t sframe[sizeof(utframe_t)];

    FD_ZERO(&wfd);

    memset(sframe, 0, sizeof(utframe_t));
    status = ut_serialize_frame_encode(frame, sframe);

    while ((wsz < sizeof(utframe_t)) && (status >= 0)) {
        status = (int)send(utraced->connfd, sframe + wsz, sizeof(utframe_t) - wsz, MSG_DONTWAIT);

        if (status > 0) {
            wsz += status;
        } else if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
            tv.tv_sec = 1;
            tv.tv_usec = 0;
            FD_SET(utraced->connfd, &wfd);

            status = select(utraced->connfd + 1, NULL, &wfd, NULL, &tv);
        } else {
            status = -1;
        }
    }

    if (wsz == sizeof(utframe_t)) {
        status = wsz;
    } else {
        status = -1;
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_terminate
 *  Description:  SIGINT handler to terminate application
 * =====================================================================================
 */
static void utraced_terminate(int signum)
{
    utrace_add_error(UTRACED_TERMINATE, NULL, 0, true);
    utrace_close();

    g_utraced->running = false;

    if (signum > 0) {
        pthread_join(g_utraced->readth, NULL);
        /* uninit will do a stop first */
        transfer_uninit(&g_utraced->ft);
    }

    if (g_utraced->systrace) {
        fclose(g_utraced->systrace);
        g_utraced->systrace = NULL;
    }

    if (g_utraced->rfd > 0) {
        close(g_utraced->rfd);
    }

    if (g_utraced->connfd > 0) {
        close(g_utraced->connfd);
    }

    /* cleanup options */
    if (g_utraced->opt.opath) {
        free(g_utraced->opt.opath);
        g_utraced->opt.opath = NULL;
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_catch_sigpipe
 *  Description:  Catch SIGPIPE
 * =====================================================================================
 */
static void utraced_catch_sigpipe(int signum)
{
    utrace_add_error(UTRACED_MONITOR_DISCONNECTED, &signum, sizeof(int), true);
    g_utraced->con = false;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_getsystime
 *  Description:  Return systime for utrace client.
 * =====================================================================================
 */
void utraced_getsystime(uint64_t *ct)
{
    *ct = (uint64_t)time(NULL);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_daemonize
 *  Description:  UTraceD daemonize
 * =====================================================================================
 */
static void utraced_daemonize(void)
{
    pid_t pid, sid;
    int fd;

    /* already a daemon */
    if (getppid() == 1) {
        return;
    }

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS); /* Killing the Parent Process */
    }
    /* At this point we are executing as the child process */

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory. */
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    fd = open("/dev/null", O_RDWR, 0);
    if (fd != -1) {
        dup2(fd, STDIN_FILENO);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);

        if (fd > 2) {
            close(fd);
        }
    }

    /* resettign File Creation Mask */
    umask(027);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_create_pidfile
 *  Description:  UTraceD create the pidfile
 * =====================================================================================
 */
static int utraced_create_pidfile(void)
{
    FILE *file = NULL;
    int status = 0;
    int pid = -1;

    file = fopen(UTRACED_PIDFILE_PATH, "w");
    if (file == NULL) {
        return (-1);
    } else {
        pid = getpid();
        if (!fprintf(file, "%d\n", pid)) {
            status = -1;
        }
        fclose(file);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  UTraceD main function.
 * =====================================================================================
 */
int main(int argc, char *argv[])
{
    struct sockaddr_in serv_addr;
    int long_index = 0;
    bool help = false;
    utraced_t *utraced;
    struct timeval tv;
    int listenfd = 0;
    fd_set rfd;
    int status;
    int port;
    int c;

    utraced = (utraced_t *)malloc(sizeof(utraced_t));
    memset(utraced, 0, sizeof(utraced_t));

    /*	options	descriptor */
    struct option longopts[] = {{"port", required_argument, NULL, 'p'},
                                {"output", required_argument, NULL, 'o'},
                                {"split", no_argument, NULL, 's'},
                                {"limit", required_argument, NULL, 'l'},
                                {"help", no_argument, NULL, 'h'},
                                {"daemon", no_argument, NULL, 'd'},
                                {NULL, 0, NULL, 0}};

    while ((c = getopt_long(argc, argv, "p:o:s::d::c:l:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'p':
            utraced->opt.port = (int)atoi(optarg);
            break;

        case 'o':
            utraced->opt.opath = strdup(optarg);
            break;

        case 'd':
            utraced->opt.daemon = true;
            break;

        case 's':
            utraced->opt.split = true;
            break;

        case 'l':
            utraced->opt.limit = (uint32_t)atoi(optarg);
            break;

        case 'h':
            help = true;
            break;

        default:
            break;
        }
    }

    if ((utraced->opt.split == true) && (utraced->opt.limit < 1)) {
        help = true;
    }

    if (help) {
        printf("UTraceD %s: UTrace deamon application\n\n", UTRACEVERSION);
        printf("Usage: utraced [OPTIONS] \n\n");
        printf("  General:\n");
        printf("     --port, -p      <number>    Port to listen for monitor connection\n");
        printf("     --output, -o    <path>      Trace output directory\n");
        printf("     --daemon, -d                Application should daemonize\n");
        printf("     --split, -s                 Split files when the size limit is reached\n");
        printf("     --limit, -l     <size>      Size in MB for trace file size limit\n\n");
        printf("  Help:\n");
        printf("     --help, -h                  Print this help\n\n");
        exit(EXIT_SUCCESS);
    }

    /* daemonize if requested */
    if (utraced->opt.daemon) {
        utraced_daemonize();
        utraced_create_pidfile();
    }

    signal(SIGINT, utraced_terminate);
    signal(SIGTERM, utraced_terminate);
    signal(SIGPIPE, utraced_catch_sigpipe);

    if (utraced_init(utraced) < 0) {
        exit(EXIT_FAILURE);
    }

    g_utraced = utraced;

    utrace_init(UTRACEAPPID, (uint32_t)getpid());
    utrace_settime_cb(&utraced_getsystime);
    utrace_open();

    if (pthread_create(&utraced->readth, NULL, utraced_read_loop, (void *)utraced)) {
        exit(EXIT_FAILURE);
    }

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    /* if a port option is available use it if not use default */
    port = utraced->opt.port > 0 ? utraced->opt.port : UT_UTRACED_PORT;
    serv_addr.sin_port = htons(port);

    bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    listen(listenfd, 1);

    FD_ZERO(&rfd);

    while (utraced->running) {
        tv.tv_sec = 3;
        tv.tv_usec = 0;
        FD_SET(listenfd, &rfd);

        status = select(listenfd + 1, &rfd, NULL, NULL, &tv);

        if (status < 0) {
            utrace_add_error(UTRACED_MAIN_SELECT_FAILED, NULL, 0, true);
        } else if (status) {
            utraced->connfd = accept(listenfd, (struct sockaddr *)NULL, NULL);

            if (utraced->connfd > 0) {
                utraced->con = true;
                utrace_add_error(UTRACED_MONITOR_CONNECTED, NULL, 0, true);
            }
        } else {
            /* no new monitor connected */
        }
    }

    utraced_terminate(0);

    exit(EXIT_SUCCESS);
}
