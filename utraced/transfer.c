/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "transfer.h"
#include "ut_client_app.h"
#include "utracedefs.h"
#include <libgen.h>

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static void *transfer_run(void *p);

static void transfer_write(transfer_t *ft, utframe_t *frame);

static int transfer_send_file(transfer_t *ft, utframe_t *frame);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_init
 *  Description:  Initialize transfer module
 * =====================================================================================
 */
int transfer_init(transfer_t *ft)
{
    int status = 0;

    if (ft == NULL) {
        return (-1);
    }

    frame_list_init(&ft->frames);
    pthread_mutex_init(&ft->cmtx, NULL);
    pthread_cond_init(&ft->cnd, NULL);
    ft->write_cb = NULL;
    ft->run = false;

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_start
 *  Description:  Start thread in transfer module
 * =====================================================================================
 */
int transfer_start(transfer_t *ft)
{
    int status = 0;

    ft->run = true;

    if (pthread_create(&ft->th, NULL, transfer_run, (void *)ft)) {
        exit(EXIT_FAILURE);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_stop
 *  Description:  Stop thread in transfer module
 * =====================================================================================
 */
int transfer_stop(transfer_t *ft)
{
    int status = 0;

    ft->run = false;

    pthread_mutex_lock(&ft->cmtx);
    pthread_cond_signal(&ft->cnd);
    pthread_mutex_unlock(&ft->cmtx);

    pthread_join(ft->th, NULL);

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_uninit
 *  Description:  Uninitialize transfer module
 * =====================================================================================
 */
int transfer_uninit(transfer_t *ft)
{
    int status = 0;

    if (ft->run == true) {
        transfer_stop(ft);
    }

    pthread_mutex_destroy(&ft->cmtx);
    pthread_cond_destroy(&ft->cnd);

    ft->write_cb = NULL;

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_push
 *  Description:  Push a frame for transfer module
 * =====================================================================================
 */
int transfer_push(transfer_t *ft, utframe_t *frame)
{
    int status = 0;

    status = frame_list_add(&ft->frames, frame);
    if (status >= 0) {
        pthread_mutex_lock(&ft->cmtx);
        pthread_cond_signal(&ft->cnd);
        pthread_mutex_unlock(&ft->cmtx);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_set_write_callback
 *  Description:  Set the write callback for single frame
 * =====================================================================================
 */
void transfer_set_write_callback(transfer_t *ft, void *cb)
{
    ft->write_cb = cb;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_write
 *  Description:  Write a single frame
 * =====================================================================================
 */
static void transfer_write(transfer_t *ft, utframe_t *frame)
{
    if (ft->write_cb) {
        ft->write_cb(frame);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_send_file
 *  Description:  Send a complete file starting with frame markers
 *                The tms field is used as frame counter and we send all frames from here
 *                The pid of all frames will be a unique identifier
 * =====================================================================================
 */
static int transfer_send_file(transfer_t *ft, utframe_t *frame)
{
    FILE *input = NULL;
    int status = 0;
    uint64_t bytecount = 0;
    uint64_t framecount = 0;
    utframe_t outframe;
    uint64_t filesize = 0;

    memset(&outframe, 0, sizeof(utframe_t));

    outframe.sof = UTSOF;
    outframe.typ = utffst;
    outframe.aid = UTRACEDID;
    outframe.pid = (uint32_t)rand();
    outframe.tms = 0; /* we use the tms as frame counter */

    input = fopen((char *)&frame->pld.txt, "r");
    if (input != NULL) {
        /* Send the start of file transfer frame  */
        outframe.typ = utffst;
        outframe.tms = (uint64_t)time(NULL);

        fseek(input, 0L, SEEK_END);
        filesize = (uint64_t)ftell(input);
        sprintf((char *)&outframe.pld.txt,
                "%s;%lu",
                basename((char *)&frame->pld.txt),
                (unsigned long)filesize);

        transfer_write(ft, &outframe);

        /* Send the payload frame by frame */
        fseek(input, 0L, SEEK_SET);
        outframe.typ = utffpl;
        while (!feof(input)) {
            memset(&outframe.pld.dat, 0, UT_FRMDAT_SZ);
            bytecount = bytecount + fread(&outframe.pld.dat, 1, UT_FRMDAT_SZ, input);
            transfer_write(ft, &outframe);
            outframe.tms = (uint64_t)time(NULL);
            framecount++;
        }

        /* Send the end of file transfer frame  */
        outframe.typ = utffso;
        outframe.tms = (uint64_t)time(NULL);
        memset(&outframe.pld.txt, 0, UT_FRMDAT_SZ);
        sprintf((char *)&outframe.pld.txt,
                "%lu;%lu",
                (unsigned long)bytecount,
                (unsigned long)(framecount - 1));
        transfer_write(ft, &outframe);

        /* Close the file stream now */
        fclose(input);
    } else {
        utrace_add_error(UTRACED_FT_EACCES, NULL, 0, NULL);
    }

    return (status);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  transfer_run
 *  Description:  Main transfer thread loop
 * =====================================================================================
 */
static void *transfer_run(void *p)
{
    transfer_t *ft = (transfer_t *)p;
    utframe_t *frame;

    frame = (utframe_t *)malloc(sizeof(utframe_t));

    while (ft->run == true) {
        pthread_mutex_lock(&ft->cmtx);
        pthread_cond_wait(&ft->cnd, &ft->cmtx);

        memset(frame, 0, sizeof(utframe_t));

        while ((frame_list_rem(&ft->frames, frame) >= 0) && (ft->run == true)) {
            transfer_send_file(ft, frame);
        }

        pthread_mutex_unlock(&ft->cmtx);
    }

    pthread_exit(NULL);
}
