/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef FILETRANSFER_H
#define FILETRANSFER_H

#include "framelist.h"
#include "ut_types.h"
#include <pthread.h>
#include <string.h>
#include <unistd.h>

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct transfer {
    bool run;
    framel_t frames;
    pthread_t th;
    pthread_mutex_t cmtx;
    pthread_cond_t cnd;
    int (*write_cb)(utframe_t *frame);
} transfer_t;

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */
int transfer_init(transfer_t *ft);

int transfer_start(transfer_t *ft);

int transfer_stop(transfer_t *ft);

int transfer_uninit(transfer_t *ft);

int transfer_push(transfer_t *ft, utframe_t *frame);

void transfer_set_write_callback(transfer_t *ft, void *cb);

#endif /* FILETRANSFER_H */
