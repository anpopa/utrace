/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "utracegen.h"
#include "ut_types.h"
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int c;
    bool isfile = false;
    char inputf[100];
    char incline[1024];
    char syscall[120];
    FILE *outf = NULL;
    int long_index = 0;

    /*	options	descriptor */
    struct option longopts[] = {{"file", required_argument, NULL, 'f'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    memset(inputf, 0, sizeof(inputf));
    memset(incline, 0, sizeof(incline));
    memset(syscall, 0, sizeof(syscall));

    while ((c = getopt_long(argc, argv, "f:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'f':
            strncpy(inputf, optarg, sizeof(inputf));
            isfile = true;
            break;

        case 'h':
        default:
            break;
        }
    }

    if (!isfile) {
        printf("UTraceGen %s: Generate UTrace decoder file\n\n", UTRACEVERSION);
        printf("Usage: utracegen --file <utracedefs.h>\n\n");
        printf("  Options:\n");
        printf("    --file, -f    <path>     Input utracedefs.h file to use\n\n");
        printf("  Help:\n");
        printf("    --help, -h               Print this help\n\n");

        exit(EXIT_SUCCESS);
    }

    unlink(UTRACEDUMMYSRC);
    unlink(UTRACEDUMMYAPP);

    outf = fopen(UTRACEDUMMYSRC, "w");

    if (outf != NULL) {
        sprintf(incline,
                "#define UTRACEGEN 1 \n\n#include <stdio.h>\n#include <stdint.h> \
					 \n#include <stdlib.h>\n#include <string.h>\n\n%s\n\n#include \"%s\"\n\n",
                utracetypesdef,
                inputf);
        fwrite(incline, sizeof(char), strlen(incline), outf);
        fwrite(utracefilesource, sizeof(char), strlen(utracefilesource), outf);
        fclose(outf);

        sprintf(syscall, "cc %s -o %s", UTRACEDUMMYSRC, UTRACEDUMMYAPP);

        if (system(syscall) >= 0) {
            sprintf(syscall, "./%s", UTRACEDUMMYAPP);
            if (system(syscall) < 0) {
                printf("utracegen: cannot execute intermediate binary\n");
            }
        } else {
            printf("utracegen: cannot compile intermediate binary\n");
        }

        unlink(UTRACEDUMMYSRC);
        unlink(UTRACEDUMMYAPP);
    } else {
        perror("Error opening file");
    }

    exit(EXIT_SUCCESS);
}
