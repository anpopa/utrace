include_directories(${utracesys_SOURCE_DIR}/utracegen)
include_directories(${utracesys_SOURCE_DIR}/libutrace)

add_executable(utracegen utracegen.c)
target_link_libraries(utracegen utracesys pthread)

install(TARGETS utracegen DESTINATION bin)
