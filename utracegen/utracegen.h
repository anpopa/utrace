/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#ifndef UTRACEGEN_H
#define UTRACEGEN_H

#define UTRACEDUMMYSRC "utracedummygen.c"
#define UTRACEDUMMYAPP "utracedummygen"

static const char utracetypesdef[] = "\
#define UT_APPNAME_LEN      32\n\
#define UT_HEADLINE_LEN     110\n\
#define UT_DESC_LEN  110\n\
#define UT_FILENAME_LEN     255\n\
\n\
#pragma pack(push, 1)\n\
typedef struct {\n\
	uint16_t hsh;\n\
	uint16_t aid;\n\
	char hline[UT_HEADLINE_LEN];\n\
	char desc[UT_DESC_LEN];\n\
	char name[UT_APPNAME_LEN];\n\
} utentry_t;\n\
#pragma pack(pop)\n\
";

static const char utracefilesource[] = "\
static int ut_serialize_uint16_encode(uint16_t dat, unsigned char *output, int *counter) {\n\
	int size = 0;\n\
\n\
	output[size++] = (unsigned char)(dat>>8);\n\
	output[size++] = (unsigned char)dat;\n\
	if (counter) *counter += size;\n\
\n\
	return size;\n\
}\n\
\n\
static int ut_serialize_entry_encode(utentry_t *entry, unsigned char *output) {\n\
	int i;\n\
	int size = 0;\n\
\n\
	ut_serialize_uint16_encode(entry->hsh, &output[size], &size);\n\
	ut_serialize_uint16_encode(entry->aid, &output[size], &size);\n\
	for (i = 0; i < UT_HEADLINE_LEN; i++)\n\
		output[size++] = (unsigned char) entry->hline[i];\n\
\n\
	for (i = 0; i < UT_DESC_LEN; i++)\n\
		output[size++] = (unsigned char) entry->desc[i];\n\
\n\
	for (i = 0; i < UT_APPNAME_LEN; i++) \n\
		output[size++] = (unsigned char) entry->name[i];\n\
\n\
	return size;\n\
}\n\
\n\
int main()\n\
{\n\
  FILE *out;\n\
  char fname[40];\n\
  unsigned char sentry[sizeof(utentry_t)];\n\
  int i;\n\
\n\
  memset(fname, 0, sizeof(fname));\n\
  sprintf(fname, \"%s.utracedecoder\", UTRACEAPPNAME);\n\
  out = fopen(fname, \"w\");\n\
  for (i=0; i<UTRACEENTRYEND; i++) {\n\
		memset(sentry, 0, sizeof(utentry_t));\n\
	  ut_serialize_entry_encode(&utraceentries[i], sentry);\n\
    fwrite(sentry, sizeof(utentry_t), 1, out);\n\
	}\n\
  fclose(out);\n\n\
  exit(EXIT_SUCCESS);\n\
 }\n";
#endif /* UTRACEGEN_H */
