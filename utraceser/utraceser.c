/*
 * =====================================================================================
 * SPDX license identifier: BSD-2-Clause
 *
 * Copyright (C) 2019 Alin Popa <alin.popa@fxdata.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALIN POPA AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * =====================================================================================
 */

#include "utraceser.h"
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
static utraceserop_t utraceser_check_action_string(const char *str);

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraceser_encode
 *  Description:  Encode input file and write data to output file
 * =====================================================================================
 */
static int utraceser_encode(utraceser_t *utraceser)
{
    int status = 0;
    uint8_t sframe[sizeof(utframe_t)];
    utframe_t frame;

    while (!feof(utraceser->input)) {
        memset(sframe, 0, sizeof(utframe_t));
        memset(&frame, 0, sizeof(utframe_t));

        status = (int)fread(&frame, sizeof(utframe_t), 1, utraceser->input);

        if (status > 0) {
            ut_serialize_frame_encode(&frame, sframe);
            fwrite(sframe, sizeof(utframe_t), 1, utraceser->output);
        }
    }

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraceser_decode
 *  Description:  Decode input file and write data to output file
 * =====================================================================================
 */
static int utraceser_decode(utraceser_t *utraceser)
{
    int status = 0;
    uint8_t sframe[sizeof(utframe_t)];
    utframe_t frame;

    while (!feof(utraceser->input)) {
        memset(sframe, 0, sizeof(utframe_t));
        memset(&frame, 0, sizeof(utframe_t));

        status = (int)fread(sframe, sizeof(utframe_t), 1, utraceser->input);

        if (status > 0) {
            ut_serialize_frame_decode(sframe, &frame);
            fwrite(&frame, sizeof(utframe_t), 1, utraceser->output);
        }
    }

    return (0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  utraced_check_bool_string
 *  Description:  Return bool based on string match for yes and no
 * =====================================================================================
 */
static utraceserop_t utraceser_check_action_string(const char *str)
{
    int ret;

    if (strcmp(str, "ENC") == 0) {
        ret = uencode;
    } else if (strcmp(str, "enc") == 0) {
        ret = uencode;
    } else if (strcmp(str, "Enc") == 0) {
        ret = uencode;
    } else if (strcmp(str, "DEC") == 0) {
        ret = udecode;
    } else if (strcmp(str, "dec") == 0) {
        ret = udecode;
    } else if (strcmp(str, "Dec") == 0) {
        ret = udecode;
    } else {
        ret = uinvalid;
    }

    return (ret);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Main function for UTraceSer tool
 * =====================================================================================
 */
int main(int argc, char *argv[])
{
    int c;
    utraceser_t utraceser;
    char ifname[UT_FILENAME_LEN + 1];
    char ofname[UT_FILENAME_LEN + 1];
    bool help = false;
    bool ifile = false;
    bool ofile = false;
    int long_index = 0;

    /*	options	descriptor */
    struct option longopts[] = {{"action", required_argument, NULL, 'a'},
                                {"input", required_argument, NULL, 'i'},
                                {"output", required_argument, NULL, 'o'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    memset(ifname, 0, UT_FILENAME_LEN + 1);
    memset(ofname, 0, UT_FILENAME_LEN + 1);

    while ((c = getopt_long(argc, argv, "a:i:o:h", longopts, &long_index)) != -1) {
        switch (c) {
        case 'a':
            utraceser.op = utraceser_check_action_string(optarg);
            break;

        case 'i':
            strncpy(ifname, optarg, UT_FILENAME_LEN);
            ifile = true;
            break;

        case 'o':
            strncpy(ofname, optarg, UT_FILENAME_LEN);
            ofile = true;
            break;

        case 'h':
        default:
            help = true;
            break;
        }
    }

    if ((utraceser.op != uencode) && (utraceser.op != udecode)) {
        help = true;
    }

    if (!ifile || !ofile) {
        help = true;
    }

    utraceser.input = fopen(ifname, "r");

    if (!utraceser.input) {
        printf("utraceser: cannot open input file %s\n", ifname);
        help = true;
    }

    utraceser.output = fopen(ofname, "w+");

    if (!utraceser.output) {
        printf("utraceser: cannot open output file %s\n", ofname);
        help = true;
    }

    if (help) {
        printf("UTraceSer %s: Serialize system utrace file\n\n", UTRACEVERSION);
        printf("Usage: utraceser --action <DEC|ENC> --input <input> --outpur <output>\n\n");
        printf("  Options:\n");
        printf("    --action, -a    ENC|DEC    Type of action to perform on input\n");
        printf("    --input, -i     <path>     Input file patch\n");
        printf("    --output, -o    <path>     Ouput file patch\n\n");
        printf("  Help:\n");
        printf("    --help, -h                 Print this help\n\n");
        exit(EXIT_SUCCESS);
    }

    switch (utraceser.op) {
    case uencode:
        utraceser_encode(&utraceser);
        break;

    case udecode:
        utraceser_decode(&utraceser);
        break;

    default:
        break;
    }

    exit(EXIT_SUCCESS);
}
